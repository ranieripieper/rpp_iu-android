package com.doisdoissete.iu.teacher.view;

import android.os.Bundle;

import com.doisdoissete.iu.pro.R;
import com.doisdoissete.iu.teacher.util.SharedPrefManager;
import com.doisdoissete.iu.teacher.view.base.BaseFragmentActivity;
import com.doisdoissete.iu.teacher.view.navigation.Navigator;

public class MainActivity extends BaseFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //if (BuildConfig.DEBUG) {
        //    Navigator.navigateToTourFragment(getContext());
        //} else {
            if (SharedPrefManager.getInstance().viewTour()) {
                if (SharedPrefManager.getInstance().getTeacher() != null) {
                    Navigator.navigateToHomeFragment(getContext());
                } else {
                    Navigator.navigateToLoginFragment(getContext());
                }
            } else {
                Navigator.navigateToTourFragment(getContext());
            }
        //}
    }

    @Override
    protected int getLayoutFragmentContainer() {
        return R.id.layout_fragment_container;
    }
}
