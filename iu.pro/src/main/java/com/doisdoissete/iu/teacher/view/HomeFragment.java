package com.doisdoissete.iu.teacher.view;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.doisdoissete.iu.cache.StudentCacheManager;
import com.doisdoissete.iu.cache.model.StudentCache;
import com.doisdoissete.iu.library.model.Student;
import com.doisdoissete.iu.library.model.Teacher;
import com.doisdoissete.iu.library.model.response.StudentsResponse;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.doisdoissete.iu.library.service.interfaces.StudentService;
import com.doisdoissete.iu.pro.R;
import com.doisdoissete.iu.teacher.util.SharedPrefManager;
import com.doisdoissete.iu.teacher.view.adapter.StudentAdapter;
import com.doisdoissete.iu.teacher.view.base.BaseFragment;
import com.doisdoissete.iu.teacher.view.navigation.Navigator;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 3/30/16.
 */
public class HomeFragment extends BaseFragment implements StudentAdapter.StudentAdapterListener {

    @Bind(R.id.lbl_msg_comecar)
    TextView mLblMsgComecar;

    @Bind(R.id.scroll_view)
    ScrollView mScrollView;

    @Bind(R.id.layout_comecar)
    View mLayoutComecar;

    @Bind(R.id.layout_items)
    View mLayoutItems;

    @Bind(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @Bind(R.id.swipe_refresh_layout)
    PtrClassicFrameLayout mPtrClassicFrameLayout;

    @Bind(R.id.swipe_refresh_layout_list)
    PtrClassicFrameLayout mPtrClassicFrameLayoutList;

    private String mMessage;

    StudentAdapter mStudentAdapter;

    Teacher mTeacher;

    private TOOLBAR_TYPE mToolbarType = TOOLBAR_TYPE.NOTHING;

    private Student mItemSelected = null;

    public static HomeFragment newInstance(String message) {
        HomeFragment homeFragment = new HomeFragment();
        homeFragment.mMessage = message;
        return homeFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTeacher = SharedPrefManager.getInstance().getTeacher();
        initRecyclerView();
        callStudentsService();

        if (!TextUtils.isEmpty(mMessage)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showMessage(mMessage);
                    mMessage = null;
                }
            }, 500);
        }
    }

    private void callStudentsService() {
        mStudentAdapter.removeAllItems();
        if (SharedPrefManager.getInstance().updateStudents()) {
            showLoadingView();
            mRecyclerView.setVisibility(View.GONE);
            mLayoutComecar.setVisibility(View.GONE);
            mLblMsgComecar.setText(getString(R.string.msg_comecar, mTeacher.getName()));
            mLayoutComecar.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
            mLayoutItems.setVisibility(View.GONE);
            mPtrClassicFrameLayoutList.setVisibility(View.GONE);
            mPtrClassicFrameLayout.setVisibility(View.GONE);

            String where = String.format("{\"instructors\":{\"$inQuery\":{\"where\":{\"objectId\":\"%s\"},\"className\":\"_User\"}}}}", mTeacher.getObjectId());
            Call<StudentsResponse> service = RetrofitManager.getInstance().getStudentService().getStudent(where, StudentService.DEFAULT_ORDER);
            service.enqueue(new Callback<StudentsResponse>() {
                @Override
                public void onResponse(Call<StudentsResponse> call, Response<StudentsResponse> response) {
                    hideLoadingView();
                    if (response.errorBody() == null) {
                        processResult(response.body());
                    } else {
                        showError(response, retryClick);
                    }
                }

                @Override
                public void onFailure(Call<StudentsResponse> call, Throwable t) {
                    showError(t, retryClick);
                }
            });
        } else {
            processResultCache(StudentCacheManager.getInstance().getAll(getRealm()));
        }

    }

    View.OnClickListener retryClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callStudentsService();
        }
    };

    private void initRecyclerView() {

        mPtrClassicFrameLayout.setRotation(180);
        mPtrClassicFrameLayoutList.setRotation(180);
        mScrollView.setRotation(180);

        View header = LayoutInflater.from(getContext()).inflate(R.layout.include_exit, null);
        header.setRotation(180);
        mPtrClassicFrameLayout.setHeaderView(header);
        mPtrClassicFrameLayout.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, mScrollView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                mPtrClassicFrameLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        logout();
                    }
                }, 150);
            }
        });

        View header1 = LayoutInflater.from(getContext()).inflate(R.layout.include_exit, null);
        header1.setRotation(180);
        mPtrClassicFrameLayoutList.setHeaderView(header1);
        mPtrClassicFrameLayoutList.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return !ViewCompat.canScrollVertically(mRecyclerView, 1);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                mPtrClassicFrameLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        logout();
                    }
                }, 150);
            }
        });

        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        //mLayoutManager.setReverseLayout(true);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mStudentAdapter = new StudentAdapter(new ArrayList<Student>(), this);
        mRecyclerView.setAdapter(mStudentAdapter);
        mRecyclerView.setRotation(180);
    }

    @OnClick(R.id.bt_comecar)
    void comecarClick() {
        Navigator.navigateToAddStudentFragment(getContext(), false);
    }

    private void processResultCache(List<StudentCache> lstStudentCache) {
        if (isAdded()) {
            if (lstStudentCache != null && !lstStudentCache.isEmpty()) {
                List<Student> lstStudent = new ArrayList<>();
                for (StudentCache studentCache : lstStudentCache) {
                    lstStudent.add(studentCache.toStudent());
                }
                processResult(lstStudent);
            } else {
                callStudentsService();
            }
            showScreen();
        }
    }

    private void processResult(StudentsResponse response) {
        if (isAdded()) {
            if (response != null && !response.isEmpty()) {
                processResult(response.getResults());
                StudentCacheManager.getInstance().updateWithoutNullsStudent(getRealm(), response.getResults());
            }
            showScreen();
        }
    }

    private void processResult(List<Student> lstStudent) {
        mStudentAdapter.addData(lstStudent);
        SharedPrefManager.getInstance().setUpdateStudents(false);
    }

    private void showScreen() {
        if (isAdded()) {
            if (mStudentAdapter != null && mStudentAdapter.getItemCount() <= 0) {
                mToolbarType = TOOLBAR_TYPE.NOTHING;
                mLblMsgComecar.setText(getString(R.string.msg_comecar, mTeacher.getName()));
                mLayoutComecar.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
                mLayoutItems.setVisibility(View.GONE);
                mPtrClassicFrameLayoutList.setVisibility(View.GONE);
                mPtrClassicFrameLayout.setVisibility(View.VISIBLE);
            } else {
                mToolbarType = TOOLBAR_TYPE.CUSTOM_BACK_BUTTON;
                mLayoutComecar.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                mLayoutItems.setVisibility(View.VISIBLE);
                mPtrClassicFrameLayoutList.setVisibility(View.VISIBLE);
                mPtrClassicFrameLayout.setVisibility(View.GONE);
            }
            configToolbar();
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return mToolbarType;
    }

    @Override
    protected Drawable getCustomDrawableBackMode() {
        if (mItemSelected == null) {
            return ResourcesCompat.getDrawable(getResources(), R.drawable.ic_add, null);
        } else {
            return ResourcesCompat.getDrawable(getResources(), R.drawable.ic_trash, null);
        }
    }

    @Override
    protected View.OnClickListener getCustomDrawableBackModeListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemSelected == null) {
                    Navigator.navigateToAddStudentFragment(getContext(), true);
                } else {
                    showDialogRemoveStudent();
                }
            }
        };
    }

    @Override
    public void onClickStudent(Student student, int position) {
        if (isAdded()) {
            mItemSelected = null;
            if (mStudentAdapter != null) {
                mStudentAdapter.cleanSelectedPosition();
            }
            configToolbar();
            Navigator.navigateToStudentDetailFragment(getContext(), student);
        }
    }

    @Override
    public void onLongClickStudent(Student student, int position) {
        if (mItemSelected != null && mItemSelected.equals(student)) {
            clearSelection();
        } else {
            mItemSelected = student;
            configToolbar();
        }
    }

    private void clearSelection() {
        mItemSelected = null;
        if (mStudentAdapter != null) {
            mStudentAdapter.cleanSelectedPosition();
        }
        configToolbar();
    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();
        callStudentsService();
    }

    private void showDialogRemoveStudent() {

        showDialog(getString(R.string.remover_estudante_dialog_msg), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSelection();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeStudent();
            }
        }, getString(R.string.cancel), getString(R.string.delete));
    }

    private void removeStudent() {
        showLoadingView();
        Call<Void> service = RetrofitManager.getInstance().getStudentService().removeStudent(mItemSelected.getObjectId());
        service.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (isAdded()) {
                    if (response != null && response.errorBody() == null) {
                        Student removeStudent = mItemSelected;
                        mItemSelected = null;
                        mStudentAdapter.removeItem(removeStudent);
                        StudentCacheManager.getInstance().delete(getRealm(), removeStudent.getObjectId());
                    } else {
                        showError(response);
                    }
                    hideLoadingView();
                    showScreen();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                showError(t);
                hideLoadingView();
            }
        });
    }

}
