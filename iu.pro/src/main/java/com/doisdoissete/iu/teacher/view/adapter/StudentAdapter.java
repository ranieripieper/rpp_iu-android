package com.doisdoissete.iu.teacher.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.doisdoissete.iu.library.helper.ClassHelper;
import com.doisdoissete.iu.library.model.Student;
import com.doisdoissete.iu.pro.R;

import java.util.List;

/**
 * Created by ranipieper on 3/24/16.
 */
public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder> {
    private List<Student> mDataset;

    private Integer mSelectedPosition = null;
    private StudentAdapterListener mListener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mLblName;
        public View mLayoutRow;

        public ViewHolder(View v) {
            super(v);
            mLblName = (TextView) v.findViewById(R.id.lbl_nome);
            mLayoutRow = v.findViewById(R.id.layout_row);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public StudentAdapter(List<Student> myDataset, StudentAdapterListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public StudentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_student, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Student student = mDataset.get(position);

        holder.mLblName.setText(ClassHelper.getMonth(student.getName()));

        holder.mLayoutRow.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                refreshSelectedPosition(position);
                if (mListener != null) {
                    mListener.onLongClickStudent(student, position);
                    return true;
                }
                return false;
            }
        });

        holder.mLayoutRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClickStudent(student, position);
                }
            }
        });

        if (mSelectedPosition != null && position == mSelectedPosition) {
            holder.mLayoutRow.setBackgroundResource(R.color.rowSelected);
        } else {
            holder.mLayoutRow.setBackgroundResource(R.drawable.general_selector);
        }
    }

    public void removeItem(Student obj) {
        if (this.mDataset != null && obj != null) {

            int position = this.mDataset.indexOf(obj);
            if (position >= 0) {
                notifyItemRemoved(position);
                this.mDataset.remove(obj);
                if (mSelectedPosition != null && mSelectedPosition == position) {
                    cleanSelectedPosition();
                }
            }
        }
    }

    public void removeAllItems() {
        if (this.mDataset != null) {
            notifyDataSetChanged();
            this.mDataset.removeAll(this.mDataset);
        }
    }

    public Integer getSelectedPosition() {
        return mSelectedPosition;
    }

    public void cleanSelectedPosition() {
        refreshSelectedPosition(null);
    }

    private void refreshSelectedPosition(Integer newSelectedPosition) {
        if (mSelectedPosition != null) {
            int removeSelect = mSelectedPosition;
            mSelectedPosition = null;
            notifyItemChanged(removeSelect);
        }
        mSelectedPosition = newSelectedPosition;
        if (mSelectedPosition != null) {
            notifyItemChanged(mSelectedPosition);
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void addData(List data) {
        if (null == data || data.isEmpty()) {
            return;
        }

        int startPosition = getItemCount();
        mDataset.addAll(data);

        notifyItemRangeInserted(startPosition, getItemCount());
    }

    public interface StudentAdapterListener {
        void onClickStudent(Student student, int position);

        void onLongClickStudent(Student student, int position);
    }
}