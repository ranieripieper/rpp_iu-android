package com.doisdoissete.iu.teacher.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.doisdoissete.iu.library.model.Teacher;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.google.gson.JsonSyntaxException;

public class SharedPrefManager {

    private static SharedPrefManager sharedPrefManager;
    private Context mContext;

    private static final String PREFERENCES = SharedPrefManager.class + "";

    private static final String TEACHER = "TEACHER";
    private static final String VIEW_TOUR = "VIEW_TOUR";
    private static final String UPDATE_STUDENTS = "UPDATE_STUDENTS";

    private SharedPrefManager(Context context) {
        this.mContext = context;
    }

    public static SharedPrefManager getInstance() {
        return sharedPrefManager;
    }

    public static void init(Application application) {
        sharedPrefManager = new SharedPrefManager(application);
    }

    private SharedPreferences getSharedPreferences() {
        return mContext.getSharedPreferences(PREFERENCES, 0);
    }

    public Teacher getTeacher() {
        String json = getSharedPreferences().getString(TEACHER, null);
        try {
            if (!TextUtils.isEmpty(json)) {
                return RetrofitManager.getGson().fromJson(json, Teacher.class);
            }
        } catch (JsonSyntaxException e) {
        }
        return null;
    }

    public void setTeacher(Teacher value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(TEACHER, RetrofitManager.getGson().toJson(value));
        editor.commit();
        setUpdateStudents(true);
    }

    public boolean viewTour() {
        return getSharedPreferences().getBoolean(VIEW_TOUR, false);
    }

    public void setViewTour(boolean value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(VIEW_TOUR, value);
        editor.commit();
    }

    public boolean updateStudents() {
        return getSharedPreferences().getBoolean(UPDATE_STUDENTS, true);
    }

    public void setUpdateStudents(boolean value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(UPDATE_STUDENTS, value);
        editor.commit();
    }

    private SharedPreferences.Editor getEditor() {
        return getSharedPreferences().edit();
    }

}
