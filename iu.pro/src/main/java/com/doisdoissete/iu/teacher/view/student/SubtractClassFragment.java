package com.doisdoissete.iu.teacher.view.student;

import android.text.TextUtils;

import com.doisdoissete.iu.library.model.Student;
import com.doisdoissete.iu.library.model.entry.SetDoneClassesPayBeforeEntry;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.doisdoissete.iu.library.util.DateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 4/1/16.
 */
public class SubtractClassFragment extends BaseDoneClassFragment {

    protected String mDtExpiredAt;

    protected boolean mNavigateToShare;

    public static SubtractClassFragment newInstance(Long qtdeClasses, Student student, String dtExpiredAt, boolean navigateToShare) {
        SubtractClassFragment fragment = new SubtractClassFragment();
        fragment.mQtdeClasses = qtdeClasses;
        fragment.mStudent = student;
        fragment.mNavigateToShare = navigateToShare;
        fragment.mDtExpiredAt = dtExpiredAt;
        return fragment;
    }

    protected void callServiceSave() {
        showLoadingView();

        SetDoneClassesPayBeforeEntry entry = new SetDoneClassesPayBeforeEntry();
        entry.setTeacherId(mTeacher.getObjectId());
        entry.setStudentId(mStudent.getObjectId());
        if (!TextUtils.isEmpty(mDtExpiredAt)) {
            entry.setExpiredAt(mDtExpiredAt);
        }

        List<String> dates = new ArrayList<>();
        for (Date dt : mDates) {
            dates.add(DateUtil.DATE_YEAR_MONTH_DAY.get().format(dt));
        }
        entry.setDates(dates);
        Call<Void> service = RetrofitManager.getInstance().getClassService().setDonePayBefore(entry);
        service.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                hideLoadingView();
                if (response != null && response.errorBody() == null) {
                    processSuccess();
                } else {
                    showError();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                showError();
            }
        });
    }

    @Override
    protected boolean navigateToShare() {
        return mNavigateToShare;
    }

}
