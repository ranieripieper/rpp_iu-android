package com.doisdoissete.iu.teacher.view.navigation;

import android.content.Context;
import android.content.Intent;

import com.doisdoissete.iu.library.model.Student;
import com.doisdoissete.iu.library.view.base.BaseFragment;
import com.doisdoissete.iu.library.view.base.BaseFragmentActivity;
import com.doisdoissete.iu.teacher.view.HomeFragment;
import com.doisdoissete.iu.teacher.view.MainActivity;
import com.doisdoissete.iu.teacher.view.account.CreateAccountFragment;
import com.doisdoissete.iu.teacher.view.account.LoginFragment;
import com.doisdoissete.iu.teacher.view.student.AddStudentFragment;
import com.doisdoissete.iu.teacher.view.student.PayAfterDoneFragment;
import com.doisdoissete.iu.teacher.view.student.RecentClassesFragment;
import com.doisdoissete.iu.teacher.view.student.ShareFragment;
import com.doisdoissete.iu.teacher.view.student.StudentDetailFragment;
import com.doisdoissete.iu.teacher.view.student.SubtractClassFragment;
import com.doisdoissete.iu.teacher.view.tour.TourFragment;

/**
 * Created by ranipieper on 3/15/16.
 */
public class Navigator {

    public static void navigateToMain(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, MainActivity.class));
        }
    }

    public static void navigateToLoginFragmentWithFade(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(LoginFragment.newInstance(), true, true);
        }
    }

    public static void navigateToLoginFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(LoginFragment.newInstance(), true);
        }
    }

    public static void navigateToCreateAccountFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(CreateAccountFragment.newInstance(), false);
        }
    }

    public static void navigateToHomeFragment(Context context) {
        navigateToHomeFragment(context, null);
    }

    public static void navigateToHomeFragmentWithFade(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(HomeFragment.newInstance(null), true, true);
        }
    }

    public static void navigateToHomeFragment(Context context, String message) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(HomeFragment.newInstance(message), true);
        }
    }

    public static void navigateToAddStudentFragment(Context context, boolean addOnlyOne) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(AddStudentFragment.newInstance(addOnlyOne));
        }
    }

    public static void navigateToAddNextStudentFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(null, AddStudentFragment.newInstance(false), false, true, false);
        }
    }

    public static void navigateToStudentDetailFragment(Context context, Student student) {
        navigateToStudentDetailFragment(context, student, false);
    }

    public static void navigateToStudentDetailFragment(Context context, Student student, boolean clearBackStack) {
        if (clearBackStack && context != null) {
            ((BaseFragmentActivity) context).addTwoFragment(HomeFragment.newInstance(null), StudentDetailFragment.newInstance(student));
        } else if (context != null) {
            ((BaseFragmentActivity) context).addFragment(StudentDetailFragment.newInstance(student));
        }
    }

    public static void navigateToSubtractClassFragment(Context context, BaseFragment actualFragment, Student student, long qtdeClasses, String dtExpiredAt, boolean navigateToShare) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(actualFragment, SubtractClassFragment.newInstance(qtdeClasses, student, dtExpiredAt, navigateToShare));
        }
    }

    public static void navigateToPayAfterDoneFragment(Context context, BaseFragment actualFragment, Student student, long qtdeClasses, String dtPayment, boolean navigateToShare) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(actualFragment, PayAfterDoneFragment.newInstance(qtdeClasses, student, dtPayment, navigateToShare));
        }
    }

    public static void navigateToRecentClassesFragment(Context context, Student student) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(null, RecentClassesFragment.newInstance(student));
        }
    }

    public static void navigateToShareFragment(Context context, Student student, String msg) {
        navigateToShareFragment(context, student, msg, true);
    }

    public static void navigateToShareFragment(Context context, Student student, String msg, boolean clearBackStack) {
        if (context != null) {
            if (clearBackStack) {
                ((BaseFragmentActivity) context).addTwoFragment(HomeFragment.newInstance(null), ShareFragment.newInstance(student, msg));
            } else {
                ((BaseFragmentActivity) context).addFragment(ShareFragment.newInstance(student, msg, BaseFragment.TOOLBAR_TYPE.BACK_BUTTON), clearBackStack);
            }
        }
    }

    public static void navigateToShareFragment(Context context, Student student, boolean clearBackStack) {
        navigateToShareFragment(context, student, null, clearBackStack);
    }

    public static void navigateToShareFragment(Context context, Student student) {
        navigateToShareFragment(context, student, null, true);
    }

    public static void navigateToTourFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(TourFragment.newInstance(), true);
        }
    }

}
