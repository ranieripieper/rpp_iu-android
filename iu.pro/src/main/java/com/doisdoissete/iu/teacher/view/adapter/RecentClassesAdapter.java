package com.doisdoissete.iu.teacher.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.doisdoissete.iu.library.helper.ClassHelper;
import com.doisdoissete.iu.library.model.MonthClazz;
import com.doisdoissete.iu.pro.R;

import java.util.List;

/**
 * Created by ranipieper on 3/24/16.
 */
public class RecentClassesAdapter extends RecyclerView.Adapter<RecentClassesAdapter.ViewHolder> {
    private List<MonthClazz> mDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mLblMes;
        public TextView mLblDatas;

        public ViewHolder(View v) {
            super(v);
            mLblMes = (TextView) v.findViewById(R.id.lbl_mes);
            mLblDatas = (TextView) v.findViewById(R.id.lbl_datas);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecentClassesAdapter(List<MonthClazz> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecentClassesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_recent_classes, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MonthClazz monthClazz = mDataset.get(position);

        holder.mLblMes.setText(ClassHelper.getMonth(monthClazz.getYearMonth()));
        holder.mLblDatas.setText(ClassHelper.getDatas(monthClazz.getClasses()));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void addData(List data) {
        if (null == data || data.isEmpty()) {
            return;
        }

        int startPosition = getItemCount();
        mDataset.addAll(data);

        notifyItemRangeInserted(startPosition, getItemCount());
    }
}