package com.doisdoissete.iu.teacher.util;

/**
 * Created by ranipieper on 3/30/16.
 */
public class Constants extends com.doisdoissete.iu.library.util.Constants {

    public static final String DEBUG_NOME = "ranieri";
    public static final String DEBUG_EMAIL = "rani.pieper@doisdoissete.com";
    public static final String DEBUG_SENHA = "123456";
    public static final String DEBUG_TELEFONE = "11976444919";

    public static final int REQUEST_SUBTRACT_CLASS = 105;
    public static final String EXTRA_OBJECT_IDS = "EXTRA_OBJECT_IDS";

}
