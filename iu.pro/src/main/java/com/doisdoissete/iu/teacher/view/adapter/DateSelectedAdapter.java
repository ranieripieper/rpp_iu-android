package com.doisdoissete.iu.teacher.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.doisdoissete.iu.library.util.DateUtil;
import com.doisdoissete.iu.pro.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ranieripieper on 4/28/16.
 */
public class DateSelectedAdapter extends BaseAdapter {

    private Context mContext;
    private List<Date> mDates = new ArrayList<>();
    private DateSelectedAdapterListener mDateSelectedAdapterListener;
    public DateSelectedAdapter(Context c, DateSelectedAdapterListener dateSelectedAdapterListener) {
        mContext = c;
        mDateSelectedAdapterListener = dateSelectedAdapterListener;
    }

    public int getCount() {
        return mDates.size();
    }

    public Object getItem(int position) {
        return mDates.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewHolder viewHolder;
        if (convertView == null) {
            grid = inflater.inflate(R.layout.row_date_selected, null);
            viewHolder = new ViewHolder(grid);
            grid.setTag(viewHolder);
        } else {
            grid = convertView;
            viewHolder = (ViewHolder)grid.getTag();
        }

        viewHolder.textView.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(mDates.get(position)));

        viewHolder.layoutRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDateSelectedAdapterListener != null) {
                    mDateSelectedAdapterListener.removeDate(mDates.get(position), position);
                }
            }
        });
        return grid;
    }

    public void addDate(Date date) {
        mDates.add(date);
        notifyDataSetChanged();
    }

    public void removeDate(Date date) {
        mDates.remove(date);
        notifyDataSetChanged();
    }

    public interface DateSelectedAdapterListener {
        void removeDate(Date date, int position);
    }

    private class ViewHolder {
        View layoutRow;
        TextView textView;

        public ViewHolder(View view) {
            layoutRow = view.findViewById(R.id.layout_row);
            textView = (TextView) view.findViewById(R.id.lbl_date);
        }
    }
}