package com.doisdoissete.iu.teacher.view.student;

import com.doisdoissete.iu.library.model.Student;
import com.doisdoissete.iu.library.model.entry.AddClassesPayAfterEntry;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.doisdoissete.iu.library.util.DateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 4/27/16.
 */
public class PayAfterDoneFragment extends BaseDoneClassFragment {

    protected String mDtPayment;
    protected boolean mNavigateToShare;

    public static PayAfterDoneFragment newInstance(Long qtdeClasses, Student student, String dtPayment, boolean navigateToShare) {
        PayAfterDoneFragment fragment = new PayAfterDoneFragment();
        fragment.mQtdeClasses = qtdeClasses;
        fragment.mStudent = student;
        fragment.mDtPayment = dtPayment;
        fragment.mNavigateToShare = navigateToShare;
        return fragment;
    }

    @Override
    protected void callServiceSave() {
        showLoadingView();
        AddClassesPayAfterEntry entry = new AddClassesPayAfterEntry();
        entry.setTeacherId(mTeacher.getObjectId());
        entry.setStudentId(mStudent.getObjectId());
        entry.setDtPayment(mDtPayment);
        List<String> dates = new ArrayList<>();
        for (Date dt : mDates) {
            dates.add(DateUtil.DATE_YEAR_MONTH_DAY.get().format(dt));
        }
        entry.setDates(dates);
        Call<Void> service = RetrofitManager.getInstance().getClassService().addClassesPayAfter(entry);
        service.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                hideLoadingView();
                if (response != null && response.errorBody() == null) {
                    processSuccess();
                } else {
                    showError();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                showError();
            }
        });
    }

    @Override
    protected boolean navigateToShare() {
        return mNavigateToShare;
    }
}
