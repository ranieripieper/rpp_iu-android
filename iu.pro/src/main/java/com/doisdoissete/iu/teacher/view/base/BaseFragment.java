package com.doisdoissete.iu.teacher.view.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.doisdoissete.iu.cache.StudentCacheManager;
import com.doisdoissete.iu.library.model.Teacher;
import com.doisdoissete.iu.pro.R;
import com.doisdoissete.iu.teacher.util.SharedPrefManager;
import com.doisdoissete.iu.teacher.view.navigation.Navigator;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.ValidationError;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by ranipieper on 11/11/15.
 */
public abstract class BaseFragment extends com.doisdoissete.iu.library.view.base.BaseFragment {

    private Realm mRealm;

    @Nullable
    @Bind(R.id.loading)
    ProgressBar mLoading;

    @Nullable
    @Bind(R.id.include_layout_error)
    View mCustomError;

    @Nullable
    @Bind(R.id.lbl_msg_error)
    TextView mLblError;

    @Nullable
    @Bind(R.id.lbl_action_positive)
    TextView mLblPositiveButtonError;

    @Nullable
    @Bind(R.id.lbl_action_negative)
    TextView mLblNegativeButtonError;

    protected Teacher mTeacher;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTeacher = SharedPrefManager.getInstance().getTeacher();
    }

    @Override
    protected ProgressBar getProgressBarLoading() {
        return mLoading;
    }

    @Override
    protected void executeInject(View view) {
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    protected void showMessage(int resMsg) {
        if (isAdded()) {
            showMessage(getString(resMsg));
        }
    }

    protected void showMessage(String msg) {
        if (isAdded()) {
            showError(msg);
        }
    }

    protected void showError(List<ValidationError> errors) {
        String msg = "";
        String BREAK_LINE = "\n";
        if (errors != null) {
            for (ValidationError validationError : errors) {
                if (validationError.getFailedRules() != null) {
                    for (Rule rule : validationError.getFailedRules()) {
                        msg += (rule.getMessage(getContext())) + BREAK_LINE;
                    }
                }
            }
        }
        if (TextUtils.isEmpty(msg)) {
            showError(R.string.generic_error);
        } else {
            msg.substring(0, msg.length() - BREAK_LINE.length());
            showError(msg);
        }
    }

    protected void showError(int resMsg) {
        if (isAdded()) {
            showError(getString(resMsg));
        }
        hideLoadingView();
    }

    protected void showError(String msg) {
        if (isAdded()) {
            hideLoadingView();
            if (!showCustomError(msg)) {
                Snackbar snackbar = Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG);
                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgErrorMessage, null));
                snackbar.show();
            }
        }
        hideLoadingView();
    }

    private static final int ANIMATION_DURATION = 250;
    private static final int TIME_SHOW_MSG = 750;

    private boolean showCustomError(String msg) {
        if (isAdded() && mCustomError != null && mLblError != null) {
            mCustomError.setVisibility(View.VISIBLE);
            mLblError.setText(msg);
            mCustomError.bringToFront();
            mCustomError.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            mCustomError.setTranslationY(-mCustomError.getMeasuredHeight());
            ViewCompat.animate(mCustomError).setStartDelay(0).translationY(0).setDuration(ANIMATION_DURATION).withEndAction(new Runnable() {
                @Override
                public void run() {
                    if (isAdded()) {
                        ViewCompat.animate(mCustomError).setStartDelay(TIME_SHOW_MSG).translationY(-mCustomError.getMeasuredHeight()).setDuration(ANIMATION_DURATION);
                    }
                }
            });
            return true;
        } else {
            return false;
        }

    }

    protected void logout() {
        SharedPrefManager.getInstance().setTeacher(null);
        StudentCacheManager.getInstance().deleteAll(getRealm());
        Navigator.navigateToLoginFragment(getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void initRealm() {
        if (mRealm == null || mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }
    }

    protected Realm getRealm() {
        if (mRealm == null || mRealm.isClosed()) {
            initRealm();
        }
        return mRealm;
    }

    @Override
    protected View getCustomError() {
        return mCustomError;
    }

    @Override
    protected TextView getLblError() {
        return mLblError;
    }

    @Override
    protected TextView getPositiveErrorButton() {
        return mLblPositiveButtonError;
    }

    @Override
    protected TextView getNegativeErrorButton() {
        return mLblNegativeButtonError;
    }
}
