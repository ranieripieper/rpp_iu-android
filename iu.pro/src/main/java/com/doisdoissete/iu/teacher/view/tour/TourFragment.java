package com.doisdoissete.iu.teacher.view.tour;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.doisdoissete.iu.library.util.FunctionsUtil;
import com.doisdoissete.iu.library.util.ResourceUtil;
import com.doisdoissete.iu.pro.R;
import com.doisdoissete.iu.teacher.util.SharedPrefManager;
import com.doisdoissete.iu.teacher.view.base.BaseFragment;
import com.doisdoissete.iu.teacher.view.navigation.Navigator;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;

import java.util.Random;
import java.util.Timer;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ranipieper on 4/26/16.
 */
public class TourFragment extends BaseFragment {

    private int screen = 1;
    private static final float MAX_ZOOM = 1.1f;
    private static final int MAX_SCREEN = 6;
    private static final int TIME_NEXT_ENABLED = 750;

    private static final int ALPHA_ANIMATION = 1500;
    private static final int LEFT_RIGHT_ANIMATION = 150;
    //screen 1
    private static final int DURATION_SWING_SCREEN_1 = 500;
    private static final int INTERVAL_ANIM_SCREEN_1 = DURATION_SWING_SCREEN_1 + 2000;

    //Screen 2
    private static final int SCREEN_2_ZOOM_ANIMATION_OVER_ZOOM = 50;
    private static final int SCREEN_2_ZOOM_ANIMATION = 150;
    private static final float SCREEN_2_SCALE_MIN = 0.2F;
    private static final int SCREEN_2_DELAY = 1000;
    private static final int SCREEN_2_DURATION_BOLINHAS = 20000;

    //Screen 3
    private static final int SCREEN_3_DELAY = 1000;
    private static final int SCREEN_3_HAND_DURATION = 500;

    //Screen 4
    private static final int SCREEN_4_DELAY = 1000;
    private static final int SCREEN_4_BUTTON_DURATION = 1500;

    //Screen 5
    private static final int SCREEN_5_DELAY = 1000;
    private static final int SCREEN_5_SELECTED_DURATION = 2500;

    //Screen 6
    private static final int SCREEN_6_TOOLBAR_DURATION = 2000;

    private Handler mHandler = new Handler();
    Timer mTimer = new Timer();

    @Bind(R.id.layout_content)
    View mLayoutContent;

    @Bind(R.id.lbl_title)
    TextView mLblTitle;
    @Bind(R.id.lbl_subtitle)
    TextView mLblSubtitle;
    @Bind(R.id.lbl_explain)
    TextView mLblExplain;
    @Bind(R.id.layout_center)
    View mLayoutCenter;
    @Bind(R.id.img_center)
    ImageView mImgCenter;

    @Bind(R.id.img_bolinha_1)
    ImageView mImgBolinha1;
    @Bind(R.id.img_bolinha_2)
    ImageView mImgBolinha2;
    @Bind(R.id.img_bolinha_3)
    ImageView mImgBolinha3;
    @Bind(R.id.img_bolinha_4)
    ImageView mImgBolinha4;

    @Bind(R.id.layout_screen_3)
    View mLayoutScreen3;
    @Bind(R.id.layout_default_screen)
    View mLayoutDefaultScreen;

    @Bind(R.id.img_device)
    ImageView mImgDeviceScreen3;
    @Bind(R.id.img_hand)
    ImageView mImgHandScreen3;

    //screen4
    @Bind(R.id.layout_screen_4)
    View mLayoutScreen4;
    @Bind(R.id.img_menos)
    ImageView mImgMenos;
    @Bind(R.id.img_mais)
    ImageView mImgMais;

    @Bind(R.id.txt_switcher_nr_aulas)
    TextSwitcher mTxttSwitcherNrAulas;
    private int nrAulas = 7;

    //screen 5
    @Bind(R.id.layout_screen_5)
    View mLayoutScreen5;
    @Bind(R.id.img_calendar)
    ImageView mImgCalendar;
    @Bind(R.id.img_calendar_selected)
    ImageView mImgCalendarSelected;

    //screen 6
    @Bind(R.id.layout_screen_6)
    View mLayoutScreen6;
    @Bind(R.id.bt_next)
    Button mBtNext;
    @Bind(R.id.layout_toolbar)
    View mLayoutToolbar;


    public static TourFragment newInstance() {
        TourFragment shareFragment = new TourFragment();
        return shareFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setText(mLblTitle, "tour_title_");
        setText(mLblSubtitle, "tour_subtitle_");
        setText(mLblExplain, "tour_explain_");

        //alpha first screen
        ViewCompat.animate(mLblTitle).alpha(1).setDuration(ALPHA_ANIMATION);
        ViewCompat.animate(mLblSubtitle).alpha(1).setDuration(ALPHA_ANIMATION);
        ViewCompat.animate(mLblExplain).alpha(1).setDuration(ALPHA_ANIMATION);
        ViewCompat.animate(mLayoutCenter).alpha(1).setDuration(ALPHA_ANIMATION);
        mLayoutDefaultScreen.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                screen1Animation();
            }
        }, ALPHA_ANIMATION);
    }

    private void animateBolinhas() {
        if (!isAdded()) {
            return;
        }
        mLayoutCenter.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int maxWidht = FunctionsUtil.getScreenWidht(getActivity().getWindowManager());
        int maxHeight = mLayoutCenter.getMeasuredHeight();

        mImgBolinha1.setVisibility(View.VISIBLE);
        mImgBolinha2.setVisibility(View.VISIBLE);
        mImgBolinha3.setVisibility(View.VISIBLE);
        mImgBolinha4.setVisibility(View.VISIBLE);

        if (!isAdded()) {
            return;
        }
        animateBolinha(mImgBolinha1, maxWidht, maxHeight);
        animateBolinha(mImgBolinha2, maxWidht, maxHeight);
        animateBolinha(mImgBolinha3, maxWidht, maxHeight);
        animateBolinha(mImgBolinha4, maxWidht, maxHeight);

        mLayoutDefaultScreen.bringToFront();

    }

    private void animateBolinha(ImageView img, int maxWidht, int maxHeight) {
        if (!isAdded()) {
            return;
        }
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setTarget(img);

        int maxPoints = 10;
        float[] randonTranslationX = new float[maxPoints + 1];
        float[] randonTranslationY = new float[maxPoints + 1];

        randonTranslationX[0] = getNextFloat(null, maxWidht);
        randonTranslationY[0] = getNextFloat(null, maxHeight);

        for (int i = 0; i < maxPoints / 2; i++) {
            randonTranslationX[i + 1] = getNextFloat(randonTranslationX[i], maxWidht);
            randonTranslationX[maxPoints - i] = randonTranslationX[i];

            randonTranslationY[i + 1] = getNextFloat(randonTranslationY[i], maxHeight);
            randonTranslationY[maxPoints - i] = randonTranslationY[i];
        }

        ObjectAnimator alpha = ObjectAnimator.ofFloat(img, "alpha", 1);
        alpha.setDuration(SCREEN_2_DURATION_BOLINHAS / 20);
        ObjectAnimator translationX = ObjectAnimator.ofFloat(img, "translationX", randonTranslationX);
        ObjectAnimator translationY = ObjectAnimator.ofFloat(img, "translationY", randonTranslationY);
        translationX.setDuration(SCREEN_2_DURATION_BOLINHAS);
        translationY.setDuration(SCREEN_2_DURATION_BOLINHAS);
        translationX.setRepeatCount(ValueAnimator.INFINITE);
        translationY.setRepeatCount(ValueAnimator.INFINITE);
        animatorSet.playTogether(alpha, translationX, translationY);
        animatorSet.start();
    }

    private int getNextFloat(Float anterior, float max) {
        Random ran = new Random();
        int nextInt = ran.nextInt(Float.valueOf(max).intValue());
        if (anterior != null) {
            while (Math.abs(nextInt - anterior) > max / 2) {
                nextInt = ran.nextInt(Float.valueOf(max).intValue());
            }
        }

        return nextInt;
    }

    private void showNextScreen() {
        mHandler = null;

        if (screen == 6) {
            animationLeftOutScreenRunnable(mLblTitle, null);
            animationLeftOutScreenRunnable(mLblSubtitle, null);
            animationLeftOutScreenRunnable(mLblExplain, null);
        } else {
            animationLeftOutScreen(mLblTitle, "tour_title_");
            animationLeftOutScreen(mLblSubtitle, "tour_subtitle_");
            animationLeftOutScreen(mLblExplain, "tour_explain_");
        }


        if (screen == 1) {
            screen1Animation();
        } else if (screen == 2) {
            animationLeftOutScreenRunnable(mLayoutCenter, new Runnable() {
                @Override
                public void run() {
                    prepareScreen();
                    screen2Animation();
                }
            });
        } else if (screen == 3) {
            animationLeftOutScreenRunnable(mLayoutCenter, new Runnable() {
                @Override
                public void run() {
                    prepareScreen();
                    screen3Animation();
                }
            });
        } else if (screen == 4) {
            animationLeftOutScreenRunnable(mLayoutCenter, new Runnable() {
                @Override
                public void run() {
                    prepareScreen();
                    screen4Animation();
                }
            });
        } else if (screen == 5) {
            animationLeftOutScreenRunnable(mLayoutCenter, new Runnable() {
                @Override
                public void run() {
                    prepareScreen();
                    screen5Animation();
                }
            });
        } else if (screen == 6) {
            animationLeftOutScreenRunnable(mLayoutCenter, new Runnable() {
                @Override
                public void run() {
                    prepareScreen();
                    screen6Animation();
                }
            });
        }
    }

    private void screen6Animation() {
        if (isAdded()) {
            animationLeftInScreen(mLayoutCenter, new Runnable() {
                @Override
                public void run() {

                }
            });
            animationLeftOutScreenRunnable(mBtNext, new Runnable() {
                @Override
                public void run() {
                    screen6ToolbarAnimation();
                }
            });
        }
    }

    private void screen6ToolbarAnimation() {
        int pos = mLayoutToolbar.getLeft();
        int witdh = mLayoutToolbar.getWidth();
        mLayoutToolbar.setTranslationX(-pos - witdh - 30);
        mLayoutToolbar.setVisibility(View.VISIBLE);

        animationLeftInScreen(mLayoutToolbar, new Runnable() {
            @Override
            public void run() {
                int h = mLayoutContent.getHeight();
                ViewCompat.animate(mLayoutToolbar).translationY(-h + mLayoutToolbar.getHeight()).setDuration(SCREEN_6_TOOLBAR_DURATION).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        nextClick();
                    }
                });
            }
        });
    }

    private void prepareScreen() {
        if (!isAdded()) {
            return;
        }
        if (mImgCenter.getAnimation() != null) {
            mImgCenter.getAnimation().cancel();
            mImgCenter.clearAnimation();
        }
        mLayoutScreen4.setVisibility(View.GONE);
        mLayoutScreen3.setVisibility(View.GONE);
        mLayoutScreen5.setVisibility(View.GONE);
        mLayoutScreen6.setVisibility(View.GONE);

        mLayoutDefaultScreen.setVisibility(View.VISIBLE);
        if (screen != 2) {
            mImgBolinha1.setVisibility(View.GONE);
            mImgBolinha2.setVisibility(View.GONE);
            mImgBolinha3.setVisibility(View.GONE);
            mImgBolinha4.setVisibility(View.GONE);
        }

        if (screen == 2) {
            mImgCenter.setImageResource(R.drawable.img_tour_screen_2_1);
            mImgCenter.setTag(R.drawable.img_tour_screen_2_1);
        } else if (screen == 3) {
            mLayoutDefaultScreen.setVisibility(View.GONE);
            mLayoutScreen3.setVisibility(View.VISIBLE);
        } else if (screen == 4) {
            mLayoutDefaultScreen.setVisibility(View.GONE);
            mLayoutScreen4.setVisibility(View.VISIBLE);

            mTxttSwitcherNrAulas.setFactory(new ViewSwitcher.ViewFactory() {

                public View makeView() {
                    LayoutInflater inflater = LayoutInflater.from(getActivity());
                    TextView tvContent = (TextView) inflater.inflate(R.layout.include_textview_tour, null);
                    return tvContent;
                }
            });

            mTxttSwitcherNrAulas.setText(String.valueOf(nrAulas));
        } else if (screen == 5) {
            mLayoutScreen5.setVisibility(View.VISIBLE);
            mLayoutDefaultScreen.setVisibility(View.GONE);
        } else if (screen == 6) {
            mLayoutScreen6.setVisibility(View.VISIBLE);
            mLayoutDefaultScreen.setVisibility(View.GONE);
        }
    }


    private AnimatorSet getAnimatorSetScreen4Button(ImageView img, final boolean sum) {
        if (!isAdded()) {
            return null;
        }
        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setTarget(img);
        ObjectAnimator translationY = ObjectAnimator.ofFloat(img, "scaleY", MAX_ZOOM, 1f, 0.9f, 1f);
        ObjectAnimator translationX = ObjectAnimator.ofFloat(img, "scaleX", MAX_ZOOM, 1f, 0.9f, 1f);
        animatorSet.setDuration(SCREEN_4_BUTTON_DURATION);
        animatorSet.setStartDelay(SCREEN_4_DELAY);
        translationY.setTarget(img);
        translationX.setTarget(img);
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (isAdded() && screen == 4) {
                    if (sum) {
                        nrAulas++;
                    } else {
                        nrAulas--;
                    }
                    mTxttSwitcherNrAulas.setText(String.valueOf(nrAulas));
                }

            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        animatorSet.playTogether(translationX, translationY);
        return animatorSet;
    }

    private void screen4ButtonsAnimation() {
        if (!isAdded()) {
            return;
        }
        final AnimatorSet animatorSet1 = getAnimatorSetScreen4Button(mImgMais, true);
        final AnimatorSet animatorSet2 = getAnimatorSetScreen4Button(mImgMais, true);
        final AnimatorSet animatorSet3 = getAnimatorSetScreen4Button(mImgMenos, false);
        final AnimatorSet animatorSet4 = getAnimatorSetScreen4Button(mImgMenos, false);

        if (animatorSet1 != null) {
            animatorSet1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (!isAdded() || animatorSet2 == null) {
                        return;
                    }
                    animatorSet2.start();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }

        if (animatorSet2 != null) {
            animatorSet2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (!isAdded() || animatorSet3 == null) {
                        return;
                    }
                    animatorSet3.start();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }

        if (animatorSet3 != null) {
            animatorSet3.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (!isAdded() || animatorSet4 == null) {
                        return;
                    }
                    animatorSet4.start();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
        if (!isAdded() || animatorSet1 == null) {
            return;
        }
        animatorSet1.start();
    }

    private void screen5Animation() {
        if (isAdded()) {
            animationLeftInScreen(mLayoutCenter, new Runnable() {
                @Override
                public void run() {
                    screen5SelectedAnimation();
                }
            });
        }
    }

    private void screen5SelectedAnimation() {
        if (!isAdded()) {
            return;
        }
        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setTarget(mImgCalendarSelected);
        ObjectAnimator translationY = ObjectAnimator.ofFloat(mImgCalendarSelected, "translationX", getResources().getDimension(R.dimen.calendar_selected));
        ObjectAnimator translationY2 = ObjectAnimator.ofFloat(mImgCalendarSelected, "translationX", 0);
        animatorSet.setDuration(SCREEN_5_SELECTED_DURATION);
        animatorSet.setStartDelay(SCREEN_5_DELAY);
        translationY.setTarget(mImgCalendarSelected);
        translationY2.setStartDelay(SCREEN_5_DELAY);
        translationY2.setTarget(mImgCalendarSelected);
        animatorSet.playSequentially(translationY, translationY2);
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }
            @Override
            public void onAnimationEnd(Animator animation) {
                if (isAdded() && screen == 5) {
                    screen5SelectedAnimation();
                }
            }
            @Override
            public void onAnimationCancel(Animator animation) {
            }
            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        animatorSet.start();
    }

    private void screen4Animation() {
        if (isAdded()) {
            animationLeftInScreen(mLayoutCenter, new Runnable() {
                @Override
                public void run() {
                    screen4ButtonsAnimation();
                }
            });
        }
    }

    private void screen3Animation() {
        if (isAdded()) {
            animationLeftInScreen(mLayoutCenter, new Runnable() {
                @Override
                public void run() {
                    screen3HandAnimation(mImgHandScreen3.getTop(), getResources().getDimensionPixelOffset(R.dimen.hand_after));
                }
            });
        }
    }

    private void screen3HandAnimation(final int before, final int after) {
        if (!isAdded()) {
            return;
        }

        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setTarget(mImgHandScreen3);

        mImgHandScreen3.bringToFront();

        ObjectAnimator translationYBefore = ObjectAnimator.ofFloat(mImgHandScreen3, "translationY", 0);
        ObjectAnimator translationYAfter = ObjectAnimator.ofFloat(mImgHandScreen3, "translationY", after);
        translationYBefore.setDuration(SCREEN_3_HAND_DURATION);
        translationYAfter.setDuration(SCREEN_3_HAND_DURATION);
        translationYBefore.setStartDelay(SCREEN_3_DELAY);
        translationYAfter.setStartDelay(SCREEN_3_DELAY);
        translationYBefore.addListener(getHandAnimatorListener(R.drawable.img_device_antes));
        translationYAfter.addListener(getHandAnimatorListener(R.drawable.img_device_depois));
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (isAdded() && screen == 3) {
                    animatorSet.start();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animatorSet.playSequentially(translationYAfter, translationYBefore);
        animatorSet.start();
    }

    private Animator.AnimatorListener getHandAnimatorListener(final int resource) {
        return new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (isAdded() && screen == 3) {
                    mImgDeviceScreen3.setImageResource(resource);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };
    }

    private void screen2Animation() {
        animationLeftInScreen(mLayoutCenter, new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        animateBolinhas();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                screen2ZoomAnimation();
                            }
                        }, 300);

                    }
                }, SCREEN_2_ZOOM_ANIMATION);
            }
        });
    }

    private void screen2ZoomAnimation() {
        if (!isAdded() || screen != 2) {
            return;
        }

        mLayoutDefaultScreen.bringToFront();
        if (mImgCenter.getScaleX() == 1) {
            scaleTo(SCREEN_2_SCALE_MIN, mImgCenter, SCREEN_2_ZOOM_ANIMATION, SCREEN_2_DELAY, new Runnable() {
                @Override
                public void run() {
                    screen2ZoomAnimation();
                }
            });
        } else {
            if (mImgCenter.getTag().equals(R.drawable.img_tour_screen_2_1)) {
                mImgCenter.setImageResource(R.drawable.img_tour_screen_2_2);
                mImgCenter.setTag(R.drawable.img_tour_screen_2_2);
            } else if (mImgCenter.getTag().equals(R.drawable.img_tour_screen_2_2)) {
                mImgCenter.setImageResource(R.drawable.img_tour_screen_2_3);
                mImgCenter.setTag(R.drawable.img_tour_screen_2_3);
            } else if (mImgCenter.getTag().equals(R.drawable.img_tour_screen_2_3)) {
                mImgCenter.setImageResource(R.drawable.img_tour_screen_2_1);
                mImgCenter.setTag(R.drawable.img_tour_screen_2_1);
            }
            scaleTo(1f, mImgCenter, SCREEN_2_ZOOM_ANIMATION, 0, new Runnable() {
                @Override
                public void run() {
                    screen2ZoomAnimation();
                }
            });

        }

    }

    private void scaleTo(final float scale, final View view, final int duration, final int delay, final Runnable endAction) {
        if (!isAdded()) {
            return;
        }
        final Interpolator interpolator;
        final int newDuration;
        if (scale == 1f) {
            interpolator = new AccelerateInterpolator();
            newDuration = duration / 2;
        } else {
            interpolator = new DecelerateInterpolator();
            newDuration = duration;
        }
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        view.setPivotY(view.getMeasuredHeight() / 2);
        view.setPivotX(view.getMeasuredWidth() / 2);
        if (scale != 1f) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!isAdded()) {
                        return;
                    }
                    ViewCompat.animate(view).setDuration(SCREEN_2_ZOOM_ANIMATION_OVER_ZOOM).setInterpolator(interpolator).scaleX(MAX_ZOOM).scaleY(MAX_ZOOM).withEndAction(
                            new Runnable() {
                                @Override
                                public void run() {
                                    if (!isAdded()) {
                                        return;
                                    }
                                    ViewCompat.animate(view).setInterpolator(interpolator).scaleX(scale).setDuration(newDuration).scaleY(scale).withEndAction(endAction);
                                }
                            }
                    );
                }
            }, delay);

        } else {
            if (!isAdded()) {
                return;
            }
            ViewCompat.animate(view).setDuration(newDuration).setStartDelay(delay).setInterpolator(interpolator).scaleX(MAX_ZOOM).scaleY(MAX_ZOOM).withEndAction(new Runnable() {
                @Override
                public void run() {
                    if (!isAdded()) {
                        return;
                    }
                    ViewCompat.animate(view).setInterpolator(new DecelerateInterpolator()).scaleX(1f).scaleY(1f).setDuration(SCREEN_2_ZOOM_ANIMATION_OVER_ZOOM).withEndAction(endAction);
                }
            });
        }
    }

    private void screen1Animation() {
        if (isAdded()) {
            if (screen != 1) {
                return;
            }
            YoYo.with(Techniques.Swing)
                    .duration(DURATION_SWING_SCREEN_1)
                    .withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mHandler = null;
                            mHandler = new Handler();
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    screen1Animation();
                                }
                            }, INTERVAL_ANIM_SCREEN_1);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
                    .playOn(mImgCenter);
        }
    }

    private void animationLeftOutScreen(final TextView txtView, final String nextResource) {
        if (!isAdded()) {
            return;
        }
        animationLeftOutScreenRunnable(txtView, new Runnable() {
            @Override
            public void run() {
                animationLeftInScreen(txtView, nextResource);
            }
        });
    }

    private void animationLeftOutScreenRunnable(final View view, Runnable endAction) {
        if (!isAdded()) {
            return;
        }
        int pos = view.getLeft();
        int witdh = view.getWidth();
        ViewCompat.animate(view).translationX(-pos - witdh - 30).setDuration(LEFT_RIGHT_ANIMATION).withEndAction(endAction);
    }

    private void animationLeftInScreen(TextView txtView, String nextResource) {
        if (!isAdded()) {
            return;
        }
        setText(txtView, nextResource);
        animationLeftInScreen(txtView);
    }

    private void animationLeftInScreen(View view, Runnable endAction) {
        if (!isAdded()) {
            return;
        }
        view.setTranslationX(FunctionsUtil.getScreenWidht(getActivity().getWindowManager()));
        ViewCompat.animate(view).translationX(0).setDuration(LEFT_RIGHT_ANIMATION).withEndAction(endAction);
    }

    private void animationLeftInScreen(View view) {
        animationLeftInScreen(view, null);
    }

    private void setText(TextView txtView, String nextResource) {
        if (!isAdded()) {
            return;
        }
        txtView.setText(ResourceUtil.getStringId(String.format("%s%d", nextResource, screen), getContext()));
    }

    @OnClick(R.id.bt_next)
    void nextClick() {
        if (!isAdded() || mBtNext == null) {
            return;
        }
        mBtNext.setEnabled(false);
        screen++;
        if (screen > MAX_SCREEN) {
            SharedPrefManager.getInstance().setViewTour(true);
            if (SharedPrefManager.getInstance().getTeacher() != null) {
                Navigator.navigateToHomeFragmentWithFade(getContext());
            } else {
                Navigator.navigateToLoginFragmentWithFade(getContext());
            }
        } else {
            showNextScreen();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBtNext.setEnabled(true);
                }
            }, TIME_NEXT_ENABLED);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_tour;
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.INVISIBLE;
    }

    @Override
    protected void onBackPressed() {
        stopTimer();
        super.onBackPressed();
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
        }
    }
}
