package com.doisdoissete.iu.teacher.view.student;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ScrollView;
import android.widget.TextView;

import com.doisdoissete.iu.cache.StudentCacheManager;
import com.doisdoissete.iu.cache.model.StudentCache;
import com.doisdoissete.iu.library.helper.ClassHelper;
import com.doisdoissete.iu.library.model.Student;
import com.doisdoissete.iu.library.model.Summary;
import com.doisdoissete.iu.library.model.entry.AddClassesPayBeforeEntry;
import com.doisdoissete.iu.library.model.entry.StudentServiceEntry;
import com.doisdoissete.iu.library.model.entry.UpdateClassesToPaidPayAfterEntry;
import com.doisdoissete.iu.library.model.response.SummaryResponse;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.doisdoissete.iu.library.util.DateUtil;
import com.doisdoissete.iu.library.view.base.BaseFragmentActivity;
import com.doisdoissete.iu.library.view.custom.DatePickerFragment;
import com.doisdoissete.iu.pro.R;
import com.doisdoissete.iu.teacher.util.Constants;
import com.doisdoissete.iu.teacher.view.base.BaseFragment;
import com.doisdoissete.iu.teacher.view.navigation.Navigator;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 3/31/16.
 */
public class StudentDetailFragment extends BaseFragment {

    private static final String EMPTY_DT_VALIDADE = "-";

    @Bind(R.id.layout_aulas_futuras)
    View mLayoutAulasFuturas;

    @Bind(R.id.layout_aulas_feitas_click)
    View mLayoutAulasFuturasClick;

    @Bind(R.id.layout_type)
    View mLayoutType;

    @Bind(R.id.lbl_before)
    TextView mLblBefore;

    @Bind(R.id.lbl_after)
    TextView mLblAfter;

    @Bind(R.id.lbl_sample_before)
    TextView mLblSampleBefore;

    @Bind(R.id.lbl_sample_after)
    TextView mLblSampleAfter;

    @Bind(R.id.layout_aulas_feitas)
    View mLayoutAulasRealizadas;

    @Bind(R.id.lbl_nome)
    TextView mLblNome;

    @Bind(R.id.lbl_nr_aulas)
    TextView mLblNrAulas;

    @Bind(R.id.lbl_validade)
    TextView mLblValidade;

    @Bind(R.id.lbl_datas_aulas_realizadas)
    TextView mLblDatasAulasRealizadas;

    @Bind(R.id.scroll_view)
    ScrollView mScrollView;

    @Bind(R.id.lbl_info_summary)
    TextView mLblInfoSummary;

    @Bind(R.id.bt_salvar)
    Button mBtSalvar;

    @Bind(R.id.lbl_view_more)
    TextView mLblViewMore;

    @Bind(R.id.lbl_share_student)
    TextView mLblShareStudent;

    @Bind(R.id.layout_share)
    View mLayoutShare;

    @Bind(R.id.view_sep_share)
    View mLbSepShare;

    private Student mStudent;

    private long mQtdeClass = 0l;
    private long mQtdeClassServer = 0l;

    private Date mActualDate = null;

    private Integer mType;

    private Summary mSumary;

    public static StudentDetailFragment newInstance(Student student) {
        StudentDetailFragment fragment = new StudentDetailFragment();
        fragment.mStudent = student;
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_student_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mStudent == null) {
            finish();
        }

        getSummary();

        showHideShare();
    }

    private boolean teacherInvitedStudent() {
        StudentCache studentCache = StudentCacheManager.getInstance().get(getRealm(), mStudent.getObjectId());
        if (studentCache != null) {
            return studentCache.isInvited();
        } else {
            return false;
        }
    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();
        showHideShare();
    }

    private void getSummary() {
        showLoadingView();
        StudentServiceEntry entry = new StudentServiceEntry();
        entry.setPhoneNumber(mStudent.getPhone());
        entry.setTeacherId(mTeacher.getObjectId());
        Call<SummaryResponse> service = RetrofitManager.getInstance().getStudentService().getSummary(entry);
        service.enqueue(new Callback<SummaryResponse>() {
            @Override
            public void onResponse(Call<SummaryResponse> call, Response<SummaryResponse> response) {
                if (response != null && response.errorBody() == null) {
                    processResult(response.body());
                } else {
                    showError(retryGetSummaryClick, cancelRetryClick);
                }
            }

            @Override
            public void onFailure(Call<SummaryResponse> call, Throwable t) {
                showError(retryGetSummaryClick, cancelRetryClick);
            }
        });
    }

    View.OnClickListener retryGetSummaryClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getSummary();
        }
    };

    private void showChooseType() {
        mLblBefore.setTextColor(ResourcesCompat.getColor(getResources(), R.color.lightBlueGrey, null));
        mLblAfter.setTextColor(ResourcesCompat.getColor(getResources(), R.color.lightBlueGrey, null));
        mLblSampleAfter.setTextColor(ResourcesCompat.getColor(getResources(), R.color.gray_1, null));
        mLblSampleAfter.setText(getString(R.string.sample_after, getStudentName()));
        mLblSampleBefore.setTextColor(ResourcesCompat.getColor(getResources(), R.color.gray_1, null));
        mLblSampleBefore.setText(getString(R.string.sample_before, getStudentName()));
        mLblValidade.setText(EMPTY_DT_VALIDADE);
        mLayoutType.setVisibility(View.VISIBLE);
        mLayoutAulasFuturas.setVisibility(View.GONE);
        mBtSalvar.setVisibility(View.GONE);
        mBtSalvar.setText(R.string.proximo);
        mActualDate = null;
        mType = null;
        mQtdeClass = 0;
    }

    private void showClasses() {
        mLblBefore.setTextColor(ResourcesCompat.getColor(getResources(), R.color.lightBlueGrey, null));
        mLblAfter.setTextColor(ResourcesCompat.getColor(getResources(), R.color.lightBlueGrey, null));
        mLblSampleAfter.setTextColor(ResourcesCompat.getColor(getResources(), R.color.gray_1, null));
        mLblSampleBefore.setTextColor(ResourcesCompat.getColor(getResources(), R.color.gray_1, null));

        if (com.doisdoissete.iu.library.util.Constants.TYPE_PAY_AFTER.equals(mType)) {
            mLblNome.setText(getString(R.string.xxx_paid_after, getStudentName()));
            mLblInfoSummary.setText(R.string.classes_not_paid);
            if (mActualDate == null) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MONTH, 1);
                mActualDate = cal.getTime();
            }
        } else {
            mLblNome.setText(getString(R.string.xxx_paid_before, getStudentName()));
            mLblInfoSummary.setText(R.string.avaiable_classes);
        }

        refreshQtdeAulas();
        if (mActualDate != null) {
            mLblValidade.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(mActualDate));
        }

        mLayoutType.setVisibility(View.GONE);
        mLayoutAulasFuturas.setVisibility(View.VISIBLE);
        mBtSalvar.setText(R.string.salvar);
        mBtSalvar.setVisibility(View.VISIBLE);
    }

    private String getStudentName() {
        if (mSumary != null && !TextUtils.isEmpty(mSumary.getStudentName())) {
            return mSumary.getStudentName();
        }
        return mStudent.getName();
    }

    @OnClick(R.id.lbl_after)
    void typeAfterClick() {
        mLblBefore.setTextColor(ResourcesCompat.getColor(getResources(), R.color.lightBlueGrey, null));
        mLblAfter.setTextColor(ResourcesCompat.getColor(getResources(), R.color.darkBlueGrey, null));
        mLblSampleAfter.setTextColor(ResourcesCompat.getColor(getResources(), R.color.gray_2, null));
        mLblSampleBefore.setTextColor(ResourcesCompat.getColor(getResources(), R.color.gray_1, null));
        mBtSalvar.setVisibility(View.VISIBLE);
        mType = Constants.TYPE_PAY_AFTER;
        mActualDate = null;
        mQtdeClass = 0;
    }

    @OnClick(R.id.lbl_before)
    void typeBeforeClick() {
        mLblBefore.setTextColor(ResourcesCompat.getColor(getResources(), R.color.darkBlueGrey, null));
        mLblAfter.setTextColor(ResourcesCompat.getColor(getResources(), R.color.lightBlueGrey, null));
        mLblSampleAfter.setTextColor(ResourcesCompat.getColor(getResources(), R.color.gray_1, null));
        mLblSampleBefore.setTextColor(ResourcesCompat.getColor(getResources(), R.color.gray_2, null));
        mBtSalvar.setVisibility(View.VISIBLE);
        mType = Constants.TYPE_PAY_BEFORE;
        mActualDate = null;
        mQtdeClass = 0;
    }

    private void processResult(SummaryResponse response) {
        if (!isAdded()) {
            return;
        }

        if (response != null && response.getSummary() != null) {
            mSumary = response.getSummary();
            mType = response.getSummary().getType();

            mLblShareStudent.setText(getString(R.string.msg_share_student, getStudentName()));
            if (response.getSummary().getRecentClasses() != null && !response.getSummary().getRecentClasses().isEmpty()) {
                String datas = ClassHelper.getDatas(response.getSummary().getRecentClasses());
                mLblDatasAulasRealizadas.setText(datas);
                mLayoutAulasRealizadas.setVisibility(View.VISIBLE);
                if (response.getSummary().getRecentClasses().size() >= Constants.LIMIT_RECENT_CLASSES) {
                    mLblViewMore.setVisibility(View.VISIBLE);
                    mLayoutAulasFuturasClick.setEnabled(true);
                    mLayoutAulasFuturasClick.setClickable(true);
                } else {
                    mLayoutAulasFuturasClick.setEnabled(false);
                    mLayoutAulasFuturasClick.setClickable(false);
                    mLblViewMore.setVisibility(View.GONE);
                }
            } else {
                mLayoutAulasRealizadas.setVisibility(View.GONE);
            }
            if (response.getSummary().getQtClasses() == 0l) {
                showChooseType();
            } else {
                processAvaiableClasses(response.getSummary());
                showClasses();
            }
            refreshQtdeAulas();
        }

        mScrollView.setVisibility(View.VISIBLE);
        hideLoadingView();
    }


    private void processAvaiableClasses(Summary summary) {
        if (!isAdded()) {
            return;
        }
        mQtdeClass = summary.getQtClasses();
        mQtdeClassServer = summary.getQtClasses();
        Date date = summary.getDate();

        if (date != null) {
            mLblValidade.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(date));
        } else {
            mLblValidade.setText("-");
        }
        mActualDate = date;

        mLblNrAulas.setText(String.valueOf(mQtdeClass));
        refreshQtdeAulas();
        mLayoutAulasFuturas.setVisibility(View.VISIBLE);
        mBtSalvar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void showError(Throwable t) {
        super.showError(t);
        super.hideLoadingView();
    }

    @OnClick(R.id.lbl_validade)
    void validadeOnClick() {
        DialogFragment newFragment = new DatePickerFragment(mActualDate) {
            @Override
            public String getTitle() {
                return getString(R.string.data_validade_aulas);
            }


            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.YEAR, year);
                mLblValidade.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(cal.getTime()));
            }
        };
        newFragment.show(getChildFragmentManager(), "datePicker");
    }

    @OnClick(R.id.ic_add_class)
    void addClassClick() {
        mQtdeClass++;
        refreshQtdeAulas();
    }

    @OnClick(R.id.ic_subtract_class)
    void subtractClassClick() {
        if (mQtdeClass <= 0) {
            mQtdeClass = 0;
        } else {
            mQtdeClass--;
        }
        refreshQtdeAulas();
    }

    @OnClick(R.id.bt_salvar)
    void btSalvarClick() {
        if (mLayoutType.getVisibility() == View.VISIBLE) {
            showClasses();
        } else {
            saveClasses();
        }
    }

    private void saveClasses() {
        String dtLbl = mLblValidade.getText().toString();
        if (TextUtils.isEmpty(dtLbl) || dtLbl.equals(EMPTY_DT_VALIDADE)) {
            if (Constants.TYPE_PAY_BEFORE.equals(mType)) {
                showError(R.string.data_validade_invalida);
            } else {
                showError(R.string.data_prev_pagamento_invalida);
            }
        } else {
            if (Constants.TYPE_PAY_BEFORE.equals(mType)) {
                saveClassesBefore();
            } else {
                saveClassesAfter();
            }
        }
    }

    private void saveClassesAfter() {
        String dtLbl = mLblValidade.getText().toString();
        if (mQtdeClass >= 0) {
            String date = null;

            if (mActualDate != null) {
                date = DateUtil.DATE_DAY_MONTH_YEAR.get().format(mActualDate);
            }

            if (mQtdeClass == mQtdeClassServer) {
                //verifica se alterou a data
                if (date != null && !date.equalsIgnoreCase(dtLbl) || (date == null && dtLbl != null)) {
                    callUpdateDtPay();
                }
            } else if (mQtdeClass < mQtdeClassServer) {
                callPaidClasses(mQtdeClassServer - mQtdeClass);
            } else if (mQtdeClass > mQtdeClassServer) {
                showCalendarAddClass(getDataToSend(getDataValidade()));
            }
        }
    }

    private void showCalendarAddClass(String dtPayment) {
        long qtdeSubtract = mQtdeClass - mQtdeClassServer;
        Navigator.navigateToPayAfterDoneFragment(getContext(), this, mStudent, qtdeSubtract, dtPayment, navigateToShare());
    }

    private void saveClassesBefore() {
        String dtLbl = mLblValidade.getText().toString();
        if (mQtdeClass >= 0) {

            String date = null;

            if (mActualDate != null) {
                date = DateUtil.DATE_DAY_MONTH_YEAR.get().format(mActualDate);
            }

            if (mQtdeClass >= mQtdeClassServer) {
                if (mQtdeClass > mQtdeClassServer || (mQtdeClass == mQtdeClassServer && (date != null && !date.equalsIgnoreCase(dtLbl) || (date == null && dtLbl != null)))) {
                    callAddClasses(mQtdeClass - mQtdeClassServer);
                }
            } else {
                showCalendarSubtractClasses();
            }
        }
    }

    private void callUpdateDtPay() {
        showLoadingView();
        UpdateClassesToPaidPayAfterEntry entry = new UpdateClassesToPaidPayAfterEntry();
        entry.setTeacherId(mTeacher.getObjectId());
        entry.setStudentId(mStudent.getObjectId());
        entry.setPayAt(getDataToSend(getDataValidade()));

        Call<Void> service = RetrofitManager.getInstance().getClassService().updateDtPayClassesPayAfter(entry);
        service.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                hideLoadingView();
                if (response != null && response.errorBody() == null) {
                    if (navigateToShare()) {
                        Navigator.navigateToShareFragment(getContext(), mStudent, getString(R.string.informacoes_salvas));
                    } else {
                        Navigator.navigateToHomeFragment(getContext(), getString(R.string.informacoes_salvas));
                    }
                } else {
                    showError();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                showError();
            }
        });
    }

    private void callPaidClasses(long qtde) {
        showLoadingView();
        UpdateClassesToPaidPayAfterEntry entry = new UpdateClassesToPaidPayAfterEntry();
        entry.setTeacherId(mTeacher.getObjectId());
        entry.setStudentId(mStudent.getObjectId());
        entry.setPayAt(getDataToSend(getDataValidade()));
        entry.setQtClasses(qtde);

        Call<Void> service = RetrofitManager.getInstance().getClassService().updateClassesToPaidPayAfter(entry);
        service.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                hideLoadingView();
                if (response != null && response.errorBody() == null) {
                    if (navigateToShare()) {
                        Navigator.navigateToShareFragment(getContext(), mStudent, getString(R.string.informacoes_salvas));
                    } else {
                        Navigator.navigateToHomeFragment(getContext(), getString(R.string.informacoes_salvas));
                    }
                } else {
                    showError();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                showError();
            }
        });
    }

    private void callAddClasses(long qtde) {
        showLoadingView();
        AddClassesPayBeforeEntry entry = new AddClassesPayBeforeEntry();
        entry.setTeacherId(mTeacher.getObjectId());
        entry.setStudentId(mStudent.getObjectId());
        entry.setQtClasses(qtde);
        entry.setDtValidade(getDataToSend(getDataValidade()));

        Call<Void> service = RetrofitManager.getInstance().getClassService().addClassesPayBefore(entry);
        service.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                hideLoadingView();
                if (response != null && response.errorBody() == null) {
                    if (navigateToShare()) {
                        Navigator.navigateToShareFragment(getContext(), mStudent, getString(R.string.informacoes_salvas));
                    } else {
                        Navigator.navigateToHomeFragment(getContext(), getString(R.string.informacoes_salvas));
                    }
                } else {
                    showError();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                showError();
            }
        });
    }

    private boolean navigateToShare() {
        if (mSumary == null || (mSumary.getQtClasses() == 0 && (mSumary.getRecentClasses() == null || mSumary.getRecentClasses().isEmpty()))) {
            return true;
        }
        return false;
    }

    private Date getDataValidade() {
        try {
            return DateUtil.DATE_DAY_MONTH_YEAR.get().parse(mLblValidade.getText().toString());
        } catch (ParseException e) {
            return new Date();
        }
    }

    private String getDataToSend(Date date) {
        if (date != null) {
            return DateUtil.DATE_YEAR_MONTH_DAY.get().format(date);
        }
        return null;
    }

    private void showCalendarSubtractClasses() {
        long qtdeSubtract = mQtdeClassServer - mQtdeClass;
        Navigator.navigateToSubtractClassFragment(getContext(), this, mStudent, qtdeSubtract, getDataToSend(getDataValidade()), navigateToShare());
    }

    private void refreshQtdeAulas() {
        if (isAdded()) {
            mLblNrAulas.setText(String.valueOf(mQtdeClass));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_SUBTRACT_CLASS) {
            if (resultCode == Activity.RESULT_CANCELED) {
                mQtdeClass = mQtdeClassServer;
                refreshQtdeAulas();
            } else if (resultCode == Activity.RESULT_OK) {
                showLoadingView();
                /*
                String[] objectIds = data.getStringArrayExtra(Constants.EXTRA_OBJECT_IDS);
                if (objectIds != null) {
                    List<Clazz> clazzToRemove = new ArrayList<>();
                    for (String oId : objectIds) {
                        for (Clazz clazz : mClasses) {
                            if (oId.equals(clazz.getObjectId())) {
                                clazzToRemove.add(clazz);
                            }
                        }
                    }
                    mClasses.removeAll(clazzToRemove);
                }
                mQtdeClass = mClasses.size();
                */
                //TODO
                refreshQtdeAulas();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @OnClick(R.id.layout_share)
    void layoutShareClick() {
        Navigator.navigateToShareFragment(getContext(), mStudent, false);
    }

    @OnClick(R.id.layout_aulas_feitas_click)
    void aulasRealizadasClick() {
        Navigator.navigateToRecentClassesFragment(getContext(), mStudent);
    }

    @Override
    protected void onBackPressed() {
        showHideShare();
        if (mQtdeClassServer == 0 && mLayoutType.getVisibility() == View.GONE) {
            showChooseType();
        } else {
            if (getActivity() != null && ((BaseFragmentActivity)getActivity()).countFragments() <= 0) {
                Navigator.navigateToHomeFragment(getContext());
            } else {
                super.onBackPressed();
            }
        }
    }

    private void showHideShare() {
        if (isAdded()) {
            if (teacherInvitedStudent()) {
                mLayoutShare.setVisibility(View.GONE);
                mLbSepShare.setVisibility(View.VISIBLE);
            } else {
                mLayoutShare.setVisibility(View.VISIBLE);
                mLbSepShare.setVisibility(View.GONE);
            }
        }
    }


    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }

    protected boolean backButtonNavigateToPreviousFragment() {
        if (mQtdeClassServer == 0 && mLayoutType.getVisibility() == View.GONE) {
            showChooseType();
            return false;
        } else {
            return true;
        }
    }

}
