package com.doisdoissete.iu.teacher.view.student;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.doisdoissete.iu.library.model.Student;
import com.doisdoissete.iu.pro.R;
import com.doisdoissete.iu.teacher.view.adapter.DateSelectedAdapter;
import com.doisdoissete.iu.teacher.view.base.BaseFragment;
import com.doisdoissete.iu.teacher.view.navigation.Navigator;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ranipieper on 4/27/16.
 */
public abstract class BaseDoneClassFragment extends BaseFragment {

    @Bind(R.id.layout_selected_dates)
    RelativeLayout mLayoutSelectedDates;

    @Bind(R.id.gridview_selected_dates)
    GridView mGridViewSelectedDates;

    private DateSelectedAdapter mDateSelectedAdapter;

    @Bind(R.id.calendar_view)
    MaterialCalendarView mCalendarView;

    @Bind(R.id.bt_submit)
    Button mbtSubmit;

    @Bind(R.id.content)
    ScrollView mScrollView;

    protected Student mStudent;
    protected List<Date> mDates = new ArrayList<>();

    protected long mQtdeClasses = 0l;

    protected int result = Activity.RESULT_CANCELED;

    private int mCalendarHeight = 100;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        refreshSubmitButton();

        mCalendarView.setCurrentDate(Calendar.getInstance());
        mCalendarView.setSelectedDate(Calendar.getInstance());
        mCalendarView.addDecorator(new DateSelectedDecorator());
        mCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                mCalendarView.invalidateDecorators();
            }
        });
        mCalendarView.setMaximumDate(Calendar.getInstance());

        mCalendarView.post(new Runnable() {
            @Override
            public void run() {
                configureHeightGridView();
            }
        });

    }

    private void configureHeightGridView() {
        mCalendarView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        mCalendarHeight = mCalendarView.getTop() + mCalendarView.getMeasuredHeight();
    }

    private void addDate() {
        CalendarDay selectedDate = mCalendarView.getSelectedDate();
        mDates.add(selectedDate.getDate());
        if (isAdded() && mQtdeClasses != 1) {
            addDateToView(selectedDate.getDate());
            mCalendarView.clearSelection();
            mLayoutSelectedDates.invalidate();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isAdded()) {
                        mScrollView.invalidate();
                        mScrollView.fullScroll(View.FOCUS_DOWN);
                    }
                }
            }, 200);
        }

        mCalendarView.invalidateDecorators();
    }

    private void refreshSubmitButton() {
        if (mDates.size() >= mQtdeClasses - 1) {
            mbtSubmit.setText(R.string.salvar);
        } else {
            mbtSubmit.setText(R.string.proxima_aula);
        }
        mbtSubmit.setVisibility(View.VISIBLE);
    }

    private void addDateToView(final Date date) {
        if (!isAdded()) {
            return;
        }
        if (mDateSelectedAdapter == null) {
            mDateSelectedAdapter = new DateSelectedAdapter(getContext(), new DateSelectedAdapter.DateSelectedAdapterListener() {
                @Override
                public void removeDate(Date date, int position) {
                    BaseDoneClassFragment.this.removeDate(date);
                }
            });
            mGridViewSelectedDates.setAdapter(mDateSelectedAdapter);
        }

        mDateSelectedAdapter.addDate(date);
        if (mLayoutSelectedDates.getVisibility() == View.GONE) {
            mLayoutSelectedDates.setVisibility(View.INVISIBLE);
        }

        setGridViewColumnWidth();
        mLayoutSelectedDates.setVisibility(View.VISIBLE);
    }

    private void setGridViewColumnWidth() {
        if (isAdded() && getActivity() != null) {
            // Convert DIPs to pixels
            DisplayMetrics metrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int columnWidth = (int) Math.floor(95 * metrics.scaledDensity);

            Rect p = new Rect();
            mGridViewSelectedDates.getSelector().getPadding(p);
            int selectorPadding = p.left + p.right;

            int numColumns = (metrics.widthPixels - selectorPadding) / columnWidth;

            int contentWidth = numColumns * columnWidth; // Width of items
            contentWidth += selectorPadding; // Plus extra space for selector on sides

            // Now calculate amount of left and right margin so the grid gets
            // centered. This is what we
            // unfortunately cannot do with layout_width="wrap_content"
            // and layout_gravity="center_horizontal"
            int slack = metrics.widthPixels - contentWidth;

            final int nrColumnsActual = this.mDates.size();
            if (numColumns > nrColumnsActual) {
                numColumns = nrColumnsActual;
            }
            mGridViewSelectedDates.setColumnWidth(columnWidth + slack / numColumns);
            mGridViewSelectedDates.setNumColumns(numColumns);
            //mLayoutSelectedDates.setPadding(slack / 2, p.top, slack / 2, p.bottom);

            //set max height
            final int h = metrics.heightPixels - mCalendarHeight;

            //mGridViewSelectedDates.invalidate();
            //mLayoutSelectedDates.invalidate();

            final int nrColumnsFinal = numColumns;
            mLayoutSelectedDates.invalidate();
            mLayoutSelectedDates.invalidate();
            mLayoutSelectedDates.post(new Runnable() {
                @Override
                public void run() {
                    mLayoutSelectedDates.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                    if (firstH == 0) {
                        firstH = mLayoutSelectedDates.getMeasuredHeight();
                    }
                    int size = Double.valueOf(firstH * (Math.ceil(Float.valueOf(mDates.size()) / Float.valueOf(nrColumnsFinal)))).intValue();
                    if (size > h) {
                        setHeight(mGridViewSelectedDates, h);
                        setHeight(mLayoutSelectedDates, h);
                    } else {
                        setHeight(mGridViewSelectedDates, size);
                        setHeight(mLayoutSelectedDates, size);
                    }
                    if (nrColumnsFinal < mDates.size()) {
                        mGridViewSelectedDates.smoothScrollToPosition(mDates.size() - 1);
                    }
                    mLayoutSelectedDates.invalidate();
                    mLayoutSelectedDates.invalidate();
                }
            });
        }
    }

    private int firstH = 0;

    private void setHeight(View view, int h) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
        params.height = h;
        view.setLayoutParams(params);
    }

    private void removeDate(Date date) {
        if (!isAdded()) {
            return;
        }
        mDates.remove(date);
        refreshSubmitButton();

        mDateSelectedAdapter.removeDate(date);
        if (mDates.isEmpty()) {
            mLayoutSelectedDates.setVisibility(View.GONE);
        } else {
            setGridViewColumnWidth();
        }
        mCalendarView.clearSelection();
        mCalendarView.invalidateDecorators();
    }

    @OnClick(R.id.bt_submit)
    void onSubmitClick() {
        if (mCalendarView.getSelectedDate() != null) {
            addDate();
            if (mQtdeClasses == mDates.size()) {
                callServiceSave();
            }
            refreshSubmitButton();
        } else {
            showError(R.string.msg_select_date);
        }
    }

    protected abstract void callServiceSave();

    protected void processSuccess() {
        /*
        Intent i = new Intent();
        result = Activity.RESULT_OK;
        if (getTargetFragment() != null) {
            String[] objectIds = new String[mObjectIds.size()];
            mObjectIds.toArray(objectIds);
            i.putExtra(Constants.EXTRA_OBJECT_IDS, objectIds);
            getTargetFragment().onActivityResult(Constants.REQUEST_SUBTRACT_CLASS, Activity.RESULT_OK, i);
        }
        finish();
        */
        if (navigateToShare()) {
            Navigator.navigateToShareFragment(getContext(), mStudent, getString(R.string.informacoes_salvas));
        } else {
            Navigator.navigateToHomeFragment(getContext(), getString(R.string.informacoes_salvas));
        }
    }

    /*
    @Override
    protected void onBackPressed() {
        Intent i = new Intent();
        if (result == Activity.RESULT_CANCELED && getTargetFragment() != null) {
            getTargetFragment().onActivityResult(Constants.REQUEST_SUBTRACT_CLASS, result, i);
        }
        super.onBackPressed();
    }
    */

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_done_class;
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }


    public class DateSelectedDecorator implements DayViewDecorator {

        public DateSelectedDecorator() {
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            boolean dateSelected = mDates.contains(day.getDate());
            if (dateSelected &&
                    (mCalendarView.getSelectedDate() == null
                            || !mCalendarView.getSelectedDate().getDate().equals(day.getDate()))) {
                return true;
            }

            return false;
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.bg_selected_date, null));
            view.addSpan(new ForegroundColorSpan(Color.WHITE));
            // view.addSpan(new SelectedDateSpan(50, ResourcesCompat.getColor(getResources(), R.color.lightBlueGrey, null)));
        }
    }


    protected abstract boolean navigateToShare();
}
