package com.doisdoissete.iu.teacher.view.account;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.doisdoissete.iu.cache.StudentCacheManager;
import com.doisdoissete.iu.library.model.Teacher;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.doisdoissete.iu.pro.BuildConfig;
import com.doisdoissete.iu.pro.R;
import com.doisdoissete.iu.teacher.util.Constants;
import com.doisdoissete.iu.teacher.util.SharedPrefManager;
import com.doisdoissete.iu.teacher.view.base.BaseFragment;
import com.doisdoissete.iu.teacher.view.navigation.Navigator;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 3/30/16.
 */
public class CreateAccountFragment extends BaseFragment {

    @NotEmpty(messageResId = R.string.email_obrigatorio)
    @Email(messageResId = R.string.email_invalido)
    @Bind(R.id.edt_email)
    EditText mEdtEmail;

    @NotEmpty(messageResId = R.string.nome_obrigatorio)
    @Bind(R.id.edt_nome)
    EditText mEdtNome;

    @Password(min = 5, scheme = Password.Scheme.ANY, messageResId = R.string.senha_invalida)
    @Bind(R.id.edt_senha)
    EditText mEdtSenha;

    @ConfirmPassword(messageResId = R.string.conf_senha_invalida)
    @Bind(R.id.edt_conf_senha)
    EditText mEdtConfSenha;

    @Bind(R.id.bt_submit)
    Button mBtSubmit;

    public static CreateAccountFragment newInstance() {
        return new CreateAccountFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (BuildConfig.DEBUG) {
            mEdtNome.setText(Constants.DEBUG_NOME);
            mEdtEmail.setText(Constants.DEBUG_EMAIL);
            mEdtSenha.setText(Constants.DEBUG_SENHA);
            mEdtConfSenha.setText(Constants.DEBUG_SENHA);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_create_account;
    }

    @OnClick(R.id.bt_submit)
    void btSubmitClick() {
        Validator validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                callService();
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                mBtSubmit.setAlpha(0.5f);
                mBtSubmit.setEnabled(false);
            }
        });
        validator.validate();
    }

    private void callService() {
        showLoadingView();
        Teacher teacher = new Teacher();
        teacher.setEmail(mEdtEmail.getText().toString());
        teacher.setUsername(mEdtEmail.getText().toString());
        teacher.setName(mEdtNome.getText().toString());
        teacher.setPassword(mEdtSenha.getText().toString());

        Call<Teacher> service = RetrofitManager.getInstance().getTeacherService().signup(teacher);
        service.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                processServiceResult(response);
            }

            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                showError(t);
            }
        });
    }

    private void processServiceResult(Response<Teacher> response) {
        if (response.errorBody() != null) {
            showError(response);
        } else {
            showSuccess(response.body());
        }
    }

    private void showSuccess(Teacher teacher) {
        hideLoadingView();
        teacher.setName(mEdtNome.getText().toString());
        teacher.setEmail(mEdtEmail.getText().toString());
        teacher.setUsername(mEdtEmail.getText().toString());
        teacher.setName(mEdtNome.getText().toString());
        teacher.setPassword(mEdtSenha.getText().toString());
        StudentCacheManager.getInstance().deleteAll(getRealm());
        SharedPrefManager.getInstance().setTeacher(teacher);
        Navigator.navigateToHomeFragment(getContext());
    }

    @OnTextChanged(R.id.edt_nome)
    void edtNomeChanged() {
        validateForm();
    }

    @OnTextChanged(R.id.edt_senha)
    void edtSenhaChanged() {
        validateForm();
    }

    @OnTextChanged(R.id.edt_conf_senha)
    void edtConfSenhaChanged() {
        validateForm();
    }

    @OnTextChanged(R.id.edt_email)
    void edtEmailChanged() {
        validateForm();
    }

    private void validateForm() {
        Validator validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                mBtSubmit.setAlpha(1f);
                mBtSubmit.setEnabled(true);

            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                mBtSubmit.setAlpha(0.5f);
                mBtSubmit.setEnabled(false);
            }
        });
        validator.validate();
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}