package com.doisdoissete.iu.teacher;

import com.doisdoissete.iu.cache.base.RealmManager;
import com.doisdoissete.iu.library.IuAppApplicationBase;
import com.doisdoissete.iu.pro.BuildConfig;
import com.doisdoissete.iu.teacher.util.Constants;
import com.doisdoissete.iu.teacher.util.SharedPrefManager;

public class IuAppApplication extends IuAppApplicationBase {

    @Override
    public void onCreate() {
        super.onCreate();

        SharedPrefManager.init(this);

        initCache();

        SharedPrefManager.getInstance().setUpdateStudents(true);
    }

    private void initCache(){
        RealmManager.init(this, "iupro");
    }

    @Override
    public Integer getVersionCode() {
        return BuildConfig.VERSION_CODE;
    }

    @Override
    public int getAppId() {
        return Constants.IU_PRO_APP_ID;
    }
}
