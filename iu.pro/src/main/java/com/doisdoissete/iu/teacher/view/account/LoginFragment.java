package com.doisdoissete.iu.teacher.view.account;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.doisdoissete.iu.cache.StudentCacheManager;
import com.doisdoissete.iu.library.model.Teacher;
import com.doisdoissete.iu.library.model.parse.ErrorResponse;
import com.doisdoissete.iu.library.service.ErrorUtils;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.doisdoissete.iu.pro.BuildConfig;
import com.doisdoissete.iu.pro.R;
import com.doisdoissete.iu.teacher.util.Constants;
import com.doisdoissete.iu.teacher.util.SharedPrefManager;
import com.doisdoissete.iu.teacher.view.base.BaseFragment;
import com.doisdoissete.iu.teacher.view.navigation.Navigator;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 3/15/16.
 */
public class LoginFragment extends BaseFragment {


    //@NotEmpty(messageResId = R.string.email_obrigatorio)
    @Email(messageResId = R.string.email_invalido)
    @Bind(R.id.edt_email)
    EditText mEdtEmail;

    @Password(min = 3 , scheme = Password.Scheme.ANY, messageResId = R.string.senha_invalida)
    @Bind(R.id.edt_senha)
    EditText mEdtSenha;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (BuildConfig.DEBUG) {
            mEdtEmail.setText(Constants.DEBUG_EMAIL);
            mEdtSenha.setText(Constants.DEBUG_SENHA);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_login;
    }

    @OnClick(R.id.bt_submit)
    void btSubmitClick() {
        Validator validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                callLoginService();
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                showError(errors);
            }
        });
        validator.validate();
    }

    @OnClick(R.id.lbl_esqueceu_senha)
    void esqueceuSenhaClick() {
        EsqueceuSenhaValidator validator = new EsqueceuSenhaValidator(mEdtEmail, new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                callEsqueceuSenhaService();
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                showError(R.string.precisamos_email_para_ajudar);
            }
        });
        validator.validate();
    }

    private void callLoginService() {
        showLoadingView();

        Call<Teacher> service = RetrofitManager.getInstance().getTeacherService().login(mEdtEmail.getText().toString(), mEdtSenha.getText().toString());
        service.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                if (response.errorBody() == null) {
                    StudentCacheManager.getInstance().deleteAll(getRealm());
                    SharedPrefManager.getInstance().setTeacher(response.body());
                    hideLoadingView();
                    Navigator.navigateToHomeFragment(getContext());
                } else {
                    showError(response);
                }
            }

            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                showError(t);
            }
        });
    }

    private void callEsqueceuSenhaService() {
        showLoadingView();
        Teacher teacher = new Teacher();
        teacher.setEmail(mEdtEmail.getText().toString());
        Call<Void> service = RetrofitManager.getInstance().getTeacherService().requestPasswordReset(teacher);
        service.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.errorBody() == null) {
                    showMessage(R.string.mandamos_email_para_voce);
                } else {
                    showError(response);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                showError(t);
            }
        });
    }

    @OnClick(R.id.lbl_crie_sua_conta_aqui)
    void createAccountClick() {
        Navigator.navigateToCreateAccountFragment(getContext());
    }

    class EsqueceuSenhaValidator {

        @NotEmpty(messageResId = R.string.email_obrigatorio)
        @Email(messageResId = R.string.email_invalido)
        EditText mEdtEmail;

        private Validator validator;

        public EsqueceuSenhaValidator(EditText edtEmail, Validator.ValidationListener listener) {
            this.mEdtEmail = edtEmail;
            validator = new Validator(this);
            validator.setValidationListener(listener);
        }

        public void validate() {
            validator.validate();
        }
    }

    protected void showError(Response response) {
        if (isAdded()) {
            ErrorResponse errorResponse = ErrorUtils.parseError(response);
            if (errorResponse.getCode() == 202) {
                showError(getString(R.string.msg_email_cadastrado));
            } else if (errorResponse.getCode() == 101) {
                showError(getString(R.string.msg_email_senha_vinvalida));
            } else {
                super.showError(response);
            }
        }
        hideLoadingView();
    }
}
