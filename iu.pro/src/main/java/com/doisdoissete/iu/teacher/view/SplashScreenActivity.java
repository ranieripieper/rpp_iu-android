package com.doisdoissete.iu.teacher.view;

import com.doisdoissete.iu.pro.R;
import com.doisdoissete.iu.teacher.view.navigation.Navigator;

/**
 * Created by ranipieper on 4/27/16.
 */
public class SplashScreenActivity extends com.doisdoissete.iu.library.view.SplashScreenActivity {

    @Override
    protected void navigateToMain() {
        Navigator.navigateToMain(SplashScreenActivity.this);
        overridePendingTransition( 0, R.anim.screen_fade_out);
    }
}
