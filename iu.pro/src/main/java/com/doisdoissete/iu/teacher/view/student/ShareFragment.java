package com.doisdoissete.iu.teacher.view.student;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.doisdoissete.iu.cache.StudentCacheManager;
import com.doisdoissete.iu.cache.model.StudentCache;
import com.doisdoissete.iu.library.model.Student;
import com.doisdoissete.iu.library.util.FunctionsUtil;
import com.doisdoissete.iu.pro.R;
import com.doisdoissete.iu.teacher.view.base.BaseFragment;
import com.doisdoissete.iu.teacher.view.navigation.Navigator;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ranipieper on 4/26/16.
 */
public class ShareFragment extends BaseFragment {

    @Bind(R.id.img_envelope)
    ImageView mImgEnvelope;

    @Bind(R.id.bt_share)
    Button mBtShare;

    @Bind(R.id.txt_msg)
    TextView mTxtMsg;

    private String mMessage;

    private Handler mHandler = new Handler();
    Timer mTimer = new Timer();

    private Student mStudent;
    private TOOLBAR_TYPE mtoolbarType;

    private int REQUEST_CODE_SHARE = 999;

    private static final int DURATION_SWING_ENVELOPE = 500;
    private static final int INTERVAL_ANIM_ENVELOPE = DURATION_SWING_ENVELOPE + 2000;

    public static ShareFragment newInstance(Student student, String message, TOOLBAR_TYPE toolbarType) {
        ShareFragment shareFragment = new ShareFragment();
        shareFragment.mStudent = student;
        shareFragment.mMessage = message;
        shareFragment.mtoolbarType = toolbarType;
        return shareFragment;
    }

    public static ShareFragment newInstance(Student student, String message) {
        return newInstance(student, message, TOOLBAR_TYPE.NOTHING);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTxtMsg.setText(getString(R.string.msg_share, mStudent.getName()));
        mBtShare.setText(getString(R.string.bt_share, mStudent.getName()));
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startEnvelopeAnimation();
            }
        }, 500);

        if (!TextUtils.isEmpty(mMessage)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showMessage(mMessage);
                    mMessage = null;
                }
            }, 500);
        }
    }

    private void startEnvelopeAnimation() {
        mImgEnvelope.setImageResource(R.drawable.envelope);
        AnimationDrawable animationDrawable = ((AnimationDrawable) mImgEnvelope.getDrawable());
        animationDrawable.start();

        long totalDuration = 0;
        for (int i = 0; i < animationDrawable.getNumberOfFrames(); i++) {
            totalDuration += animationDrawable.getDuration(i);
        }

        mTimer.schedule(getTimerTask(), totalDuration);
    }


    private TimerTask getTimerTask() {
        return new TimerTask() {
            @Override
            public void run() {
                swingEnvelope();
                mTimer.schedule(getTimerTask(), INTERVAL_ANIM_ENVELOPE);
            }
        };
    }

    private void swingEnvelope() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (isAdded()) {
                    YoYo.with(Techniques.Swing)
                            .duration(DURATION_SWING_ENVELOPE)
                            .playOn(mImgEnvelope);
                }
            }
        });
    }

    @OnClick(R.id.bt_share)
    void btShareClick() {

        FunctionsUtil.shareUrl(getContext(), getString(R.string.share_text_url, mStudent.getName()), getString(R.string.APP_NAME));
        StudentCache studentCache = StudentCacheManager.getInstance().get(getRealm(), mStudent.getObjectId());
        if (studentCache != null) {
            studentCache.setInvited(true);
            StudentCacheManager.getInstance().put(getRealm(), studentCache);
        }
        finish();
    }

    @OnClick(R.id.txt_dont_invite_now)
    void btDontInviteNowClick() {
        if (mtoolbarType == TOOLBAR_TYPE.BACK_BUTTON) {
            finish();
        } else {
            Navigator.navigateToHomeFragment(getContext());
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_share;
    }

    @Override
    protected BaseFragment.TOOLBAR_TYPE getToolbarType() {
        return mtoolbarType;
    }

    @Override
    protected void onBackPressed() {
        if (mTimer != null) {
            mTimer.cancel();
        }
        super.onBackPressed();
    }
}
