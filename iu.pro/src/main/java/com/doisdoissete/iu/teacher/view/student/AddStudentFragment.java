package com.doisdoissete.iu.teacher.view.student;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.doisdoissete.iu.cache.StudentCacheManager;
import com.doisdoissete.iu.library.model.Student;
import com.doisdoissete.iu.library.model.entry.AddStudentEntry;
import com.doisdoissete.iu.library.model.response.StudentResponse;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.doisdoissete.iu.library.util.NumberUtil;
import com.doisdoissete.iu.pro.BuildConfig;
import com.doisdoissete.iu.pro.R;
import com.doisdoissete.iu.teacher.util.Constants;
import com.doisdoissete.iu.teacher.view.base.BaseFragment;
import com.doisdoissete.iu.teacher.view.navigation.Navigator;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import br.com.jansenfelipe.androidmask.MaskEditTextChangedListener;
import butterknife.Bind;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 3/30/16.
 */
public class AddStudentFragment extends BaseFragment {

    @NotEmpty(messageResId = R.string.nome_obrigatorio)
    @Length(min=3, messageResId = R.string.nome_invalido)
    @Bind(R.id.edt_nome)
    EditText mEdtNome;

    @NotEmpty(messageResId = R.string.telefone_obrigatorio)
    @Length(min=12, messageResId = R.string.telefone_invalido)
    @Bind(R.id.edt_phone)
    EditText mEdtPhone;

    @Bind(R.id.bt_submit)
    Button mBtSubmit;

    private boolean mAddOnlyOne = true;

    public static AddStudentFragment newInstance(boolean addOnlyOne) {
        AddStudentFragment fragment = new AddStudentFragment();
        fragment.mAddOnlyOne = addOnlyOne;
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MaskEditTextChangedListener maskTEL = new MaskEditTextChangedListener("(##)#########", mEdtPhone);
        mEdtPhone.addTextChangedListener(maskTEL);

        if (BuildConfig.DEBUG) {
            mEdtNome.setText(Constants.DEBUG_NOME);
            mEdtPhone.setText(Constants.DEBUG_TELEFONE);
        }

        //if (mAddOnlyOne) {
        //    mBtSubmit.setText(R.string.salvar);
        //} else {
            mBtSubmit.setText(R.string.proximo);
        //}
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_add_student;
    }

    @OnTextChanged(R.id.edt_nome)
    void onTextChangedNome() {
        validateForm();
    }

    @OnTextChanged(R.id.edt_phone)
    void onTextChangedPhone() {
        validateForm();
    }

    @OnClick(R.id.bt_submit)
    void btSubmitClick() {
        Validator validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                callAddStudent();
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                showError(errors);
            }
        });
        validator.validate();
    }

    private void validateForm() {
        Validator validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                mBtSubmit.setAlpha(1f);
                mBtSubmit.setEnabled(true);
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                mBtSubmit.setAlpha(0.5f);
                mBtSubmit.setEnabled(false);
            }
        });
        validator.validate();
    }


    private void callAddStudent() {
        showLoadingView();
        AddStudentEntry entry = new AddStudentEntry();
        entry.setStudentName(mEdtNome.getText().toString());
        entry.setStudentPhone(NumberUtil.getPhoneNumberStr(mEdtPhone.getText().toString()));
        entry.setTeacherId(mTeacher.getObjectId());

        Call<StudentResponse> service = RetrofitManager.getInstance().getStudentService().addStudent(entry);

        service.enqueue(new Callback<StudentResponse>() {
            @Override
            public void onResponse(Call<StudentResponse> call, Response<StudentResponse> response) {
                //if (mAddOnlyOne) {
                //    finish();
                //} else {
                //    Navigator.navigateToAddNextStudentFragment(getContext());
                //}
                if (response != null && response.errorBody() == null && response.body() != null) {
                    if (response.body().getResult() != null && response.body().getResult().isError()) {
                        if (TextUtils.isEmpty(response.body().getResult().getMsgError())) {
                            showError(response);
                        } else {
                            showError(response.body().getResult().getMsgError());
                        }
                    } else {
                        Student student = response.body().getResult();
                        student.setName(mEdtNome.getText().toString());
                        student.setPhone(NumberUtil.getPhoneNumberStr(mEdtPhone.getText().toString()));
                        StudentCacheManager.getInstance().updateWithoutNullsStudent(getRealm(), student);
                        Navigator.navigateToStudentDetailFragment(getContext(), student, true);
                    }
                } else {
                    showError(response);
                }

                hideLoadingView();
            }

            @Override
            public void onFailure(Call<StudentResponse> call, Throwable t) {
                showError(t);
            }
        });
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}
