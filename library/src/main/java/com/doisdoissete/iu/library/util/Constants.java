package com.doisdoissete.iu.library.util;

/**
 * Created by ranipieper on 4/26/16.
 */
public class Constants {

    public static final Integer TYPE_PAY_BEFORE = 1;
    public static final Integer TYPE_PAY_AFTER = 2;

    public static final int LIMIT_RECENT_CLASSES = 5;

    public static final int IU_APP_ID = 1;
    public static final int IU_PRO_APP_ID = 2;

    public static final String IU_PRO_PLAY_STORE = "https://play.google.com/store/apps/details?id=com.doisdoissete.iu.pro";
    public static final String IU_PLAY_STORE = "https://play.google.com/store/apps/details?id=com.doisdoissete.iu";

    public static final int ANIMATION_DURATION_SNACK = 250;
    public static final int TIME_SHOW_MSG_SNACK = 750;
    public static final int DELAY_START_ANIMATION_SNACK = 0;

}
