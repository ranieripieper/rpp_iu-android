package com.doisdoissete.iu.library.model.response;

import com.doisdoissete.iu.library.model.Clazz;
import com.doisdoissete.iu.library.model.parse.ErrorResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 4/1/16.
 */
public class ClassBatchOperation {

    @Expose
    @SerializedName("success")
    private Clazz success;

    @Expose
    @SerializedName("error")
    private ErrorResponse error;

    public Clazz getSuccess() {
        return success;
    }

    public void setSuccess(Clazz success) {
        this.success = success;
    }

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }
}
