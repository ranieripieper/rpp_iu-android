package com.doisdoissete.iu.library.model.entry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 4/27/16.
 */
public class AddClassesPayAfterEntry {

    @Expose
    @SerializedName("teacher_id")
    private String teacherId;

    @Expose
    @SerializedName("student_id")
    private String studentId;

    @Expose
    @SerializedName("pay_at")
    private String dtPayment;

    @Expose
    @SerializedName("dates")
    private List<String> dates;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getDtPayment() {
        return dtPayment;
    }

    public void setDtPayment(String dtPayment) {
        this.dtPayment = dtPayment;
    }

    public List<String> getDates() {
        return dates;
    }

    public void setDates(List<String> dates) {
        this.dates = dates;
    }
}
