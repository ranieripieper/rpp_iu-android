package com.doisdoissete.iu.library.service.interfaces;

import com.doisdoissete.iu.library.model.entry.AddClassesPayAfterEntry;
import com.doisdoissete.iu.library.model.entry.AddClassesPayBeforeEntry;
import com.doisdoissete.iu.library.model.entry.SetDoneClassesPayBeforeEntry;
import com.doisdoissete.iu.library.model.entry.StudentServiceEntry;
import com.doisdoissete.iu.library.model.entry.UpdateClassesToPaidPayAfterEntry;
import com.doisdoissete.iu.library.model.response.ClassCustomApiResponse;
import com.doisdoissete.iu.library.model.response.ClassesResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by ranipieper on 3/17/16.
 */
public interface ClassService {

    @GET("classes/Class")
    Call<ClassesResponse> getClasses(@Query("where") String where, @Query("limit") long limit, @Query("order") String order);

    @GET("classes/Class")
    Call<ClassesResponse> count(@Query("where") String where, @Query("count") int count, @Query("limit") long limit);

    //@POST("batch")
    //Call<List<ClassBatchOperation>> addClasses(@Body ClassBatchOperationEntry entry);

    @POST("functions/classes_get_done")
    Call<ClassCustomApiResponse> getClassesDone(@Body StudentServiceEntry params);

    @POST("functions/classes_pay_before_add_classes")
    Call<Void> addClassesPayBefore(@Body AddClassesPayBeforeEntry entry);

    @POST("functions/classes_pay_before_set_done_classes")
    Call<Void> setDonePayBefore(@Body SetDoneClassesPayBeforeEntry entry);

    @POST("functions/classes_pay_after_add_classes")
    Call<Void> addClassesPayAfter(@Body AddClassesPayAfterEntry entry);

    @POST("functions/classes_pay_after_update_to_paid")
    Call<Void> updateClassesToPaidPayAfter(@Body UpdateClassesToPaidPayAfterEntry entry);

    @POST("functions/classes_pay_after_update_dt_pay")
    Call<Void> updateDtPayClassesPayAfter(@Body UpdateClassesToPaidPayAfterEntry entry);

}
