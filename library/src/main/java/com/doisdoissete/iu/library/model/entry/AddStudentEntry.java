package com.doisdoissete.iu.library.model.entry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 3/31/16.
 */
public class AddStudentEntry {

    @Expose
    @SerializedName("random")
    private double random = Math.random();

    @Expose
    @SerializedName("student_phone")
    private String studentPhone;

    @Expose
    @SerializedName("teacher_id")
    private String teacherId;

    @Expose
    @SerializedName("student_name")
    private String studentName;

    public double getRandom() {
        return random;
    }

    public void setRandom(double random) {
        this.random = random;
    }

    public String getStudentPhone() {
        return studentPhone;
    }

    public void setStudentPhone(String studentPhone) {
        this.studentPhone = studentPhone;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
}
