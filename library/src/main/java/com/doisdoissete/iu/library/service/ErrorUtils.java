package com.doisdoissete.iu.library.service;

import com.doisdoissete.iu.library.model.parse.ErrorResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by ranipieper on 3/30/16.
 */
public class ErrorUtils {

    public static ErrorResponse parseError(Response<?> response) {
    Converter<ResponseBody, ErrorResponse> converter =
            RetrofitManager.getInstance().getRetrofitParse()
                    .responseBodyConverter(ErrorResponse.class, new Annotation[0]);

        ErrorResponse error;

    try {
        if (response.errorBody() != null) {
            error = converter.convert(response.errorBody());
        } else {
            return new ErrorResponse();
        }
    } catch (IOException e) {
        return new ErrorResponse();
    }

    return error;
}
}