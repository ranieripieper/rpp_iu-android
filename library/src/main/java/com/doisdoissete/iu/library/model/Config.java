package com.doisdoissete.iu.library.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 4/28/16.
 */
public class Config {

    @Expose
    @SerializedName("iu-pro")
    private ConfigApp configIuPro;

    @Expose
    @SerializedName("iu")
    private ConfigApp configIu;

    public ConfigApp getConfigIuPro() {
        return configIuPro;
    }

    public void setConfigIuPro(ConfigApp configIuPro) {
        this.configIuPro = configIuPro;
    }

    public ConfigApp getConfigIu() {
        return configIu;
    }

    public void setConfigIu(ConfigApp configIu) {
        this.configIu = configIu;
    }
}
