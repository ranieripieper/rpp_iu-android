package com.doisdoissete.iu.library.model.entry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 4/27/16.
 */
public class AddClassesPayBeforeEntry {

    @Expose
    @SerializedName("teacher_id")
    private String teacherId;

    @Expose
    @SerializedName("student_id")
    private String studentId;

    @Expose
    @SerializedName("qt_classes")
    private Long qtClasses;

    @Expose
    @SerializedName("dt_validade")
    private String dtValidade;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public Long getQtClasses() {
        return qtClasses;
    }

    public void setQtClasses(Long qtClasses) {
        this.qtClasses = qtClasses;
    }

    public String getDtValidade() {
        return dtValidade;
    }

    public void setDtValidade(String dtValidade) {
        this.dtValidade = dtValidade;
    }
}
