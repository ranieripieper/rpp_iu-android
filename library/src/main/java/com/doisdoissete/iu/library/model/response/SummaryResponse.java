package com.doisdoissete.iu.library.model.response;

import com.doisdoissete.iu.library.model.Summary;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 4/26/16.
 */
public class SummaryResponse {

    @Expose
    @SerializedName("result")
    private Summary summary;

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }
}
