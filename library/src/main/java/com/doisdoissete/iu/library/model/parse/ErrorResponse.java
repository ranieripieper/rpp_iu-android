package com.doisdoissete.iu.library.model.parse;

import com.google.gson.annotations.Expose;

/**
 * Created by ranipieper on 3/30/16.
 */
public class ErrorResponse {

    @Expose
    private int code;

    @Expose
    private String error;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
