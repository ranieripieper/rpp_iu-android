package com.doisdoissete.iu.library;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.CrashlyticsCore;
import com.doisdoissete.iu.library.model.Config;
import com.doisdoissete.iu.library.model.ConfigApp;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.doisdoissete.iu.library.util.SharedPrefManager;
import com.parse.Parse;

import java.util.Date;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public abstract class IuAppApplicationBase extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        initCrashlytics();

        initCalligraphy();

        //init retrofit
        initRetrofitManager();

        SharedPrefManager.init(this);

        Parse.initialize(this, BuildConfig.PARSE_APPLICATION_ID, BuildConfig.PARSE_CLIENT_KEY);
        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);

        getConfig();
    }

    private void getConfig() {
        SharedPrefManager.getInstance().setVerifyUpdate(true);
        Call<Config> service = RetrofitManager.getInstance().getConfigService().getconfig();
        service.enqueue(new Callback<Config>() {
            @Override
            public void onResponse(Call<Config> call, Response<Config> response) {
                if (response != null && response.body() != null) {
                    updateConfig(response.body());
                } else {
                    SharedPrefManager.getInstance().setVerifyUpdate(true);
                }
            }

            @Override
            public void onFailure(Call<Config> call, Throwable t) {
                SharedPrefManager.getInstance().setVerifyUpdate(true);
            }
        });
    }

    private void updateConfig(Config configServer) {
        Config config = SharedPrefManager.getInstance().getConfig();
        if (config == null) {
            SharedPrefManager.getInstance().setConfig(configServer);
            SharedPrefManager.getInstance().setVerifyUpdate(true);
        } else {
            config.setConfigIu(getConfigApp(config.getConfigIu(), configServer.getConfigIu()));
            config.setConfigIuPro(getConfigApp(config.getConfigIuPro(), configServer.getConfigIuPro()));
            SharedPrefManager.getInstance().setConfig(config);
        }
    }

    private ConfigApp getConfigApp(ConfigApp config, ConfigApp configServer) {
        ConfigApp result;
        if (config != null && configServer == null) {
            result = config;
        } else if (config == null && configServer != null) {
            result = configServer;
            SharedPrefManager.getInstance().setVerifyUpdate(true);
        } else if (config != null && configServer != null) {
            if (config.getVersion() != configServer.getVersion()) {
                result = configServer;
                SharedPrefManager.getInstance().setVerifyUpdate(true);
            } else {
                result = config;
            }
        } else {
            result = null;
        }

        if (result.getDtGetServer() == null) {
            result.setDtGetServer(new Date());
        }

        return result;

    }

    private void initCalligraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Avenir-Black.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    protected void initCrashlytics() {
        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder()
                        .disabled(BuildConfig.DEBUG)
                        .build())
                .build();
        // Initialize Fabric with the debug-disabled crashlytics.
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, crashlyticsKit, new Answers(), new Crashlytics());
        } else {
            Fabric.with(this, crashlyticsKit);
        }
    }

    private void initRetrofitManager() {
        RetrofitManager.getInstance().initialize();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public abstract Integer getVersionCode();
    public abstract int getAppId();
}
