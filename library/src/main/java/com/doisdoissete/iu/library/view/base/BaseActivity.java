package com.doisdoissete.iu.library.view.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.doisdoissete.iu.library.IuAppApplicationBase;
import com.doisdoissete.iu.library.R;
import com.doisdoissete.iu.library.model.Config;
import com.doisdoissete.iu.library.model.ConfigApp;
import com.doisdoissete.iu.library.util.Constants;
import com.doisdoissete.iu.library.util.DateUtil;
import com.doisdoissete.iu.library.util.FunctionsUtil;
import com.doisdoissete.iu.library.util.SharedPrefManager;
import com.doisdoissete.iu.library.view.custom.TouchBlackHoleView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity extends AppCompatActivity {

    private ProgressDialog progress;

    protected abstract ProgressBar getProgressBarLoading();
    protected abstract Toolbar getToolbar();
    protected abstract AppBarLayout getAppBarLayout();
    protected abstract void executeInject();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        executeInject();
        initToolbar();
    }

    protected boolean hasExtra(String key) {
        if (getIntent() != null && getIntent().getExtras() != null) {
            return getIntent().getExtras().containsKey(key);
        }

        return false;
    }

    protected boolean getExtraBoolean(String key) {
        if (hasExtra(key)) {
            return getIntent().getExtras().getBoolean(key);
        }
        return false;
    }

    protected String getExtraString(String key) {
        if (hasExtra(key)) {
            return getIntent().getExtras().getString(key);
        }
        return "";
    }

    public void hideLoadingView() {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
            progress = null;
        }
    }

    public void hideKeyboard() {
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }

    /**
     * Inicializa toolbar
     */
    private void initToolbar() {
        if (getToolbar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getToolbar().setElevation(getResources().getDimensionPixelSize(R.dimen.action_bar_elevation));
            }
            setSupportActionBar(getToolbar());

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(false);
                getSupportActionBar().setTitle(getString(R.string.app_name));
            }
        }
    }

    //Toolbar
    public void setToolbarTitle(String s) {
        if (getToolbar() != null) {
            getToolbar().setTitle(s);
        }
    }

    public void setToolbarTitle(int stringId) {
        if (getToolbar() != null) {
            getToolbar().setTitle(stringId);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    protected Context getContext() {
        return BaseActivity.this;
    }

    @Override
    public void finish() {
        hideKeyboard();
        super.finish();
    }

    protected void TODO() {
        Toast.makeText(getContext(), "TODO", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    //Update Version


    protected void showUpdateVersion() {
        if (!SharedPrefManager.getInstance().verifyUpdate()) {
            return;
        }
        Config config = SharedPrefManager.getInstance().getConfig();
        if (config != null) {
            Integer version = getVersionCode();
            if (version != null) {
                ConfigApp configApp = getConfigApp(config);
                if (configApp != null) {
                    if (version.intValue() < configApp.getVersion()) {
                        showUpdateVersion(configApp);
                    }
                }
            }
        }

        SharedPrefManager.getInstance().setVerifyUpdate(false);
    }

    private void showUpdateVersion(ConfigApp configApp) {
        String urlPlayStore = getUrlUpdate();
        if (!TextUtils.isEmpty(urlPlayStore)) {
            //verifica se tem que atualizar agora
            int difInDays = DateUtil.getDifInDays(configApp.getDtGetServer());
            if (configApp.getDaysToUpdate() == 0 || configApp.getDtGetServer() == null || configApp.getDaysToUpdate() - difInDays <= 0) {
                showUpdateVersionNow(urlPlayStore, getMsgUpdate());
            } else {
                showUpdateVersion(urlPlayStore, getMsgUpdate());
            }
        }
    }

    private String getUrlUpdate() {
        if (getApplication() != null) {
            Integer appId = getAppId();
            if (appId != null) {
                if (appId.intValue() == Constants.IU_APP_ID) {
                    return Constants.IU_PLAY_STORE;
                } else if (appId.intValue() == Constants.IU_PRO_APP_ID) {
                    return Constants.IU_PRO_PLAY_STORE;
                }
            }
        }
        return null;
    }

    private String getMsgUpdate() {
        if (getApplication() != null) {
            Integer appId = getAppId();
            if (appId != null) {
                if (appId.intValue() == Constants.IU_APP_ID) {
                    return getString(R.string.new_version_iu);
                } else if (appId.intValue() == Constants.IU_PRO_APP_ID) {
                    return getString(R.string.new_version_iu_pro);
                }
            }
        }
        return getString(R.string.new_version_iu);
    }

    private ConfigApp getConfigApp(Config config) {
        if (config != null) {
            Integer appId = getAppId();
            if (appId != null) {
                if (appId.intValue() == Constants.IU_APP_ID) {
                    return config.getConfigIu();
                } else if (appId.intValue() == Constants.IU_PRO_APP_ID) {
                    return config.getConfigIuPro();
                }
            }
        }
        return null;
    }

    private Integer getAppId() {
        if (getApplication() != null) {
            IuAppApplicationBase appApplicationBase = (IuAppApplicationBase) getApplication();
            return appApplicationBase.getAppId();
        }
        return null;
    }

    private Integer getVersionCode() {
        if (getApplication() != null) {
            IuAppApplicationBase appApplicationBase = (IuAppApplicationBase) getApplication();
            return appApplicationBase.getVersionCode();
        }
        return null;
    }

    protected void showUpdateVersion(final String url, String msg) {
        showUpdateVersion(msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FunctionsUtil.openBrowser(getContext(), url);
            }
        }, null, getString(R.string.update), getString(R.string.cancel), true);
    }

    protected void showUpdateVersionNow(final String url, String msg) {
        showUpdateVersion(msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FunctionsUtil.openBrowser(getContext(), url);
            }
        }, null, getString(R.string.update_now), null, false);
    }

    private void showUpdateVersion(String msg, final View.OnClickListener positiveButtonClick, final View.OnClickListener negativeButtonClick, String positiveButton, String negativeButton, final boolean closeSnack) {
        if (getCustomError() != null && getLblError() != null) {
            if (getCustomError().getVisibility() == View.VISIBLE) {
                return;
            }
            getLblError().setText(msg);

            if (getPositiveErrorButton() != null) {
                if (!TextUtils.isEmpty(positiveButton)) {
                    getPositiveErrorButton().setText(positiveButton);
                    if (positiveButtonClick != null) {
                        getPositiveErrorButton().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (closeSnack) {
                                    closeSnack(100);
                                }
                                positiveButtonClick.onClick(v);
                            }
                        });
                    } else {
                        getPositiveErrorButton().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (closeSnack) {
                                    closeSnack(100);
                                }
                            }
                        });
                    }
                    getCustomError().findViewById(R.id.layout_buttons).setVisibility(View.VISIBLE);
                    getPositiveErrorButton().setVisibility(View.VISIBLE);
                } else {
                    getPositiveErrorButton().setVisibility(View.GONE);
                }
            }

            if (getNegativeErrorButton() != null) {
                if (!TextUtils.isEmpty(negativeButton)) {
                    getNegativeErrorButton().setText(negativeButton);
                    if (negativeButtonClick != null) {
                        getNegativeErrorButton().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                closeSnack(100);
                                negativeButtonClick.onClick(v);
                            }
                        });
                    } else {
                        getNegativeErrorButton().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                closeSnack(100);
                            }
                        });
                    }
                    getCustomError().findViewById(R.id.layout_buttons).setVisibility(View.VISIBLE);
                    getNegativeErrorButton().setVisibility(View.VISIBLE);
                } else {
                    getNegativeErrorButton().setVisibility(View.GONE);
                }
            }

            getCustomError().measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            getCustomError().setTranslationY(-getCustomError().getMeasuredHeight());
            getCustomError().setVisibility(View.VISIBLE);
            getCustomError().bringToFront();

            if (TextUtils.isEmpty(positiveButton) && TextUtils.isEmpty(negativeButton)) {
                ViewCompat.animate(getCustomError()).setStartDelay(Constants.DELAY_START_ANIMATION_SNACK).translationY(0).setDuration(Constants.ANIMATION_DURATION_SNACK).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        closeSnack(Constants.TIME_SHOW_MSG_SNACK);
                    }
                });
            } else {
                ViewCompat.animate(getCustomError()).setStartDelay(Constants.DELAY_START_ANIMATION_SNACK).translationY(0).setDuration(Constants.ANIMATION_DURATION_SNACK).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        getTouchBlackHoleView().setVisibility(View.VISIBLE);
                        getTouchBlackHoleView().bringToFront();
                    }
                });
            }
        }
    }

    private void closeSnack(final int delay) {
        if (getCustomError() != null) {
            ViewCompat.animate(getCustomError()).setStartDelay(delay).translationY(-getCustomError().getMeasuredHeight()).setDuration(Constants.ANIMATION_DURATION_SNACK).withEndAction(new Runnable() {
                @Override
                public void run() {
                    getCustomError().setVisibility(View.GONE);
                    getTouchBlackHoleView().setVisibility(View.GONE);
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showUpdateVersion();
    }

    protected abstract View getCustomError();

    protected abstract TextView getLblError();

    protected abstract TextView getPositiveErrorButton();

    protected abstract TextView getNegativeErrorButton();

    protected abstract TouchBlackHoleView getTouchBlackHoleView();
}
