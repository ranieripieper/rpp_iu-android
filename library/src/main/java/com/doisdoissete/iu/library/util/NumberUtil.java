package com.doisdoissete.iu.library.util;

/**
 * Created by ranipieper on 3/31/16.
 */
public class NumberUtil {

    public static Long getPhoneNumber(String number) {
        return  Long.valueOf(getPhoneNumberStr(number));
    }

    public static String getPhoneNumberStr(String number) {
        if (number == null) {
            return null;
        }
        String phoneNumber = number.replaceAll("\\D+", "");
        return  phoneNumber;
    }
}
