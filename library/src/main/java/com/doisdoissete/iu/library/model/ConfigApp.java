package com.doisdoissete.iu.library.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by ranipieper on 4/28/16.
 */
public class ConfigApp {

    @Expose
    @SerializedName("android_version")
    private int version;

    @Expose
    @SerializedName("days_to_update")
    private int daysToUpdate;

    private Date dtGetServer;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getDaysToUpdate() {
        return daysToUpdate;
    }

    public void setDaysToUpdate(int daysToUpdate) {
        this.daysToUpdate = daysToUpdate;
    }

    public Date getDtGetServer() {
        return dtGetServer;
    }

    public void setDtGetServer(Date dtGetServer) {
        this.dtGetServer = dtGetServer;
    }
}
