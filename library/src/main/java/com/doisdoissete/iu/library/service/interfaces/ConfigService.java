package com.doisdoissete.iu.library.service.interfaces;

import com.doisdoissete.iu.library.model.Config;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by ranipieper on 4/28/16.
 */
public interface ConfigService {

    @GET("config.json")
    Call<Config> getconfig();
}
