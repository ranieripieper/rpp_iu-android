package com.doisdoissete.iu.library.service;

import com.doisdoissete.iu.library.BuildConfig;
import com.doisdoissete.iu.library.service.interfaces.ClassService;
import com.doisdoissete.iu.library.service.interfaces.ConfigService;
import com.doisdoissete.iu.library.service.interfaces.StudentService;
import com.doisdoissete.iu.library.service.interfaces.TeacherService;
import com.doisdoissete.iu.library.util.DateUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ranipieper on 1/14/16.
 */
public class RetrofitManager {

    public Retrofit mRetrofitParse;
    public Retrofit mRetrofitApi;
    final ConcurrentHashMap<Class, Object> services;
    private static RetrofitManager instance;

    private RetrofitManager() {
        services = new ConcurrentHashMap();
    }

    public static RetrofitManager getInstance() {
        if (instance == null) {
            instance = new RetrofitManager();
        }
        return instance;
    }

    public static Gson getGson() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                //.registerTypeAdapter(Date.class, dateDeserializer)
                //.registerTypeAdapter(Date.class, dateSerializer)
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();
        return gson;
    }

    public void initialize() {

        configParse();

        configApi();

    }

    private void configApi() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(logging);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                request = request.newBuilder()
                        .build();
                Response response = chain.proceed(request);
                return response;
            }
        });

        mRetrofitApi = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .client(httpClient.build())
                .build();

    }

    private void configParse() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(logging);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                request = request.newBuilder()
                        .addHeader("X-Parse-Application-Id", BuildConfig.PARSE_APPLICATION_ID)
                        .addHeader("X-Parse-REST-API-Key", BuildConfig.PARSE_REST_KEY)
                        .build();
                Response response = chain.proceed(request);
                return response;
            }
        });

        mRetrofitParse = new Retrofit.Builder()
                .baseUrl(BuildConfig.PARSE_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .client(httpClient.build())
                .build();
    }

    //Services

    public StudentService getStudentService() {
        return (StudentService)this.getServiceParse(StudentService.class);
    }

    public ClassService getClassService() {
        return (ClassService)this.getServiceParse(ClassService.class);
    }

    public TeacherService getTeacherService() {
        return (TeacherService)this.getServiceParse(TeacherService.class);
    }

    public ConfigService getConfigService() {
        return (ConfigService)this.getServiceApi(ConfigService.class);
    }

    protected <T> Object getServiceParse(Class<T> cls) {
        if(!this.services.contains(cls)) {
            this.services.putIfAbsent(cls, this.mRetrofitParse.create(cls));
        }

        return this.services.get(cls);
    }

    protected <T> Object getServiceApi(Class<T> cls) {
        if(!this.services.contains(cls)) {
            this.services.putIfAbsent(cls, this.mRetrofitApi.create(cls));
        }

        return this.services.get(cls);
    }

    static JsonDeserializer<Date> dateDeserializer = new JsonDeserializer<Date>() {
        @Override
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            if (json != null) {
                try {
                    Date dt = DATE_SERVICE_1.get().parse(json.getAsString());
                    if (dt == null) {
                        return tryOtherFormat(json);
                    }
                    return dt;
                } catch (ParseException e) {
                    return tryOtherFormat(json);
                }
            }

            return null;
        }

        private Date tryOtherFormat(JsonElement json) {
            try {
                return DATE_SERVICE_2.get().parse(json.getAsString());
            } catch (ParseException e1) {
                return tryOtherFormat2(json);
            }
        }

        private Date tryOtherFormat2(JsonElement json) {
            try {
                return DateUtil.DATE_YEAR_MONTH_DAY.get().parse(json.getAsString());
            } catch (ParseException e1) {
            }
            return null;
        }
    };


    static JsonSerializer<Date> dateSerializer = new JsonSerializer<Date>() {
        @Override
        public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext
                context) {
            return src == null ? null : new JsonPrimitive(src.getTime());
        }
    };

    private static final ThreadLocal<DateFormat> DATE_SERVICE_1 = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ") {
                public StringBuffer format(Date date, StringBuffer toAppendTo, java.text.FieldPosition pos) {
                    StringBuffer toFix = super.format(date, toAppendTo, pos);
                    return toFix.insert(toFix.length()-2, ':');
                };

                public Date parse(String text, ParsePosition pos) {
                    int indexOf = text.indexOf(':', text.length() - 4);
                    if (indexOf > 0) {
                        text = text.substring(0, indexOf) + text.substring(indexOf+1, text.length());
                        return super.parse(text, pos);
                    } else {
                        return null;
                    }

                }

            };
        }
    };

    private static final ThreadLocal<DateFormat> DATE_SERVICE_2 = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        }
    };

    public Retrofit getRetrofitParse() {
        return mRetrofitParse;
    }

    public Retrofit getRetrofitApi() {
        return mRetrofitApi;
    }

}
