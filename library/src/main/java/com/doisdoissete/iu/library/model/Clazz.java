package com.doisdoissete.iu.library.model;

import com.doisdoissete.iu.library.model.parse.RelationshipParse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by ranipieper on 3/17/16.
 */
public class Clazz {

    public static final String PATH = "/1/classes/Class";
    public static final String PATH_PUT = "/1/classes/Class/%s";

    @Expose
    @SerializedName("done")
    private boolean done;

    @Expose
    @SerializedName("expiredAt")
    private DateParse expiredAt;

    @Expose
    @SerializedName("usedAt")
    private DateParse usedAt;

    @Expose
    @SerializedName("objectId")
    private String objectId;

    @Expose
    @SerializedName("createdAt")
    private Date createdAt;

    @Expose
    @SerializedName("updatedAt")
    private Date updatedAt;

    @Expose
    @SerializedName("instructor")
    private RelationshipParse instructor;

    @Expose
    @SerializedName("student")
    private RelationshipParse student;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public DateParse getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(DateParse expiredAt) {
        this.expiredAt = expiredAt;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public RelationshipParse getInstructor() {
        return instructor;
    }

    public void setInstructor(RelationshipParse instructor) {
        this.instructor = instructor;
    }

    public RelationshipParse getStudent() {
        return student;
    }

    public void setStudent(RelationshipParse student) {
        this.student = student;
    }

    /**
     * Gets the usedAt
     *
     * @return usedAt
     */
    public DateParse getUsedAt() {
        return usedAt;
    }

    /**
     * Sets the usedAt
     *
     * @param usedAt
     */
    public void setUsedAt(DateParse usedAt) {
        this.usedAt = usedAt;
    }
}
