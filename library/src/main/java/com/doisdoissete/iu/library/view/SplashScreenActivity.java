package com.doisdoissete.iu.library.view;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.doisdoissete.iu.library.BuildConfig;

/**
 * Created by ranipieper on 4/27/16.
 */
public abstract class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.doisdoissete.iu.library.R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                navigateToMain();
                finish();
            }
        }, BuildConfig.DEBUG ? 1000 : 1500);
    }

    protected abstract void navigateToMain();
}
