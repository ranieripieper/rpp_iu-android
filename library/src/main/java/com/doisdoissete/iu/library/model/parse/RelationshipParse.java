package com.doisdoissete.iu.library.model.parse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 3/24/16.
 */
public class RelationshipParse {

    @Expose
    @SerializedName("__type")
    private String type;

    @Expose
    @SerializedName("className")
    private String className;

    @Expose
    @SerializedName("objectId")
    private String objectId;

    public RelationshipParse(String type, String className, String objectId) {
        this.type = type;
        this.className = className;
        this.objectId = objectId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }
}

