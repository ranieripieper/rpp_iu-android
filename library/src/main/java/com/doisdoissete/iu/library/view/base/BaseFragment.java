package com.doisdoissete.iu.library.view.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.doisdoissete.iu.library.R;
import com.doisdoissete.iu.library.model.parse.ErrorResponse;
import com.doisdoissete.iu.library.service.ErrorUtils;
import com.doisdoissete.iu.library.util.Constants;

import retrofit2.Response;

/**
 * Created by ranipieper on 11/11/15.
 */
public abstract class BaseFragment extends Fragment {

    private ProgressDialog progress;

    protected abstract int getLayoutResource();

    protected abstract ProgressBar getProgressBarLoading();

    private static String TAG = "BaseFragment";

    public enum TOOLBAR_TYPE {
        DRAWER,
        BACK_BUTTON,
        CUSTOM_BACK_BUTTON,
        NOTHING,
        INVISIBLE
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Closes keyboard on fragment first appearance.
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        configToolbar();
        hideKeyboard();
    }

    protected void setImageAndTextToolbar() {
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResource(), container, false);
        executeInject(view);
        return view;
    }

    protected abstract void executeInject(View view);

    public void onResumeFromBackStack() {
        configToolbar();
        hideKeyboard();
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hideKeyboard();
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && isAdded()) {
            configToolbar();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * Used by Leak Canary
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void hideKeyboard() {
        if (isAdded() && getView() != null) {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }

    protected void configToolbar() {
        if (needConfigToolbar() && isAdded()) {
            setToolbarTitle(getToolbarTitle());
            if (TOOLBAR_TYPE.BACK_BUTTON.equals(getToolbarType())) {
                setNavBackMode();
            } else if (TOOLBAR_TYPE.DRAWER.equals(getToolbarType())) {
                resetBackMode();
            } else if (TOOLBAR_TYPE.NOTHING.equals(getToolbarType())) {
                hideDrawerAndBackButton();
            } else if (TOOLBAR_TYPE.CUSTOM_BACK_BUTTON.equals(getToolbarType())) {
                setCustomNavBackMode();
            } else if (TOOLBAR_TYPE.INVISIBLE.equals(getToolbarType())) {
                setToolbarInvisible();
            }
            setImageAndTextToolbar();
        }
    }

    protected boolean needConfigToolbar() {
        return true;
    }

    /*
     Call back when user presses back button or back on the navigation.
    */
    protected void onBackPressed() {
        hideKeyboard();
    }

    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.NOTHING;
    }

    protected String getToolbarTitle() {
        return "";
    }

    public void setNavBackMode() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).setNavBackMode();
        }
    }

    public void setToolbarInvisible() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).setToolbarInvisible();
        }
    }

    public void setCustomNavBackMode() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).setNavBackMode(getCustomDrawableBackMode(), getCustomDrawableBackModeListener());
        }
    }

    public void resetBackMode() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).resetBackMode();
        }
    }

    public void hideDrawerAndBackButton() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).hideDrawerAndBackButton();
        }
    }

    public void setToolbarTitle(String s) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).setToolbarTitle(s);
        }
    }

    public void setToolbarTitle(int stringId) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).setToolbarTitle(stringId);
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (clearMenu()) {
            menu.clear();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    protected boolean clearMenu() {
        return true;
    }

    protected void finish() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    protected Drawable getCustomDrawableBackMode() {
        return ((AppCompatActivity) getActivity()).getDrawerToggleDelegate().getThemeUpIndicator();
    }

    protected View.OnClickListener getCustomDrawableBackModeListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        };
    }

    protected void TODO() {
        Toast.makeText(getActivity(), "TODO", Toast.LENGTH_SHORT).show();
    }

    protected void TODO(String msg) {
        Toast.makeText(getActivity(), "TODO " + msg, Toast.LENGTH_SHORT).show();
    }

    public void showLoadingView() {
        synchronized (TAG) {
            hideKeyboard();
            if (getProgressBarLoading() != null) {
                getProgressBarLoading().setVisibility(View.VISIBLE);
            } else if (progress == null || !progress.isShowing()) {
                progress = ProgressDialog.show(getActivity(), getString(R.string.wait),
                        getString(R.string.loading), true);
                hideKeyboard();
            }
            if (getActivity() != null) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
    }

    public void hideLoadingView() {
        synchronized (TAG) {
            if (isAdded() && getProgressBarLoading() != null) {
                getProgressBarLoading().setVisibility(View.GONE);
            }
            if (isAdded() && progress != null && progress.isShowing()) {
                progress.dismiss();
                progress = null;
            }
            if (getActivity() != null) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
    }

    protected void showError() {
        if (!isAdded()) {
            return;
        }
        showError(R.string.generic_error);
    }

    protected void showError(View.OnClickListener positiveButton, View.OnClickListener negativeButton) {
        if (!isAdded()) {
            return;
        }
        showError(R.string.generic_error, positiveButton, negativeButton, getString(R.string.retry), getString(R.string.cancel));
    }

    protected void showError(View.OnClickListener positiveButton) {
        if (!isAdded()) {
            return;
        }
        showError(R.string.generic_error, positiveButton, null, getString(R.string.retry), getString(R.string.cancel));
    }

    protected void showError(String msg, View.OnClickListener positiveButton) {
        if (!isAdded()) {
            return;
        }
        showError(msg, positiveButton, null, getString(R.string.retry), getString(R.string.cancel));
    }

    protected void showError(Response response) {
        if (isAdded()) {
            ErrorResponse errorResponse = ErrorUtils.parseError(response);
            if (errorResponse != null && !TextUtils.isEmpty(errorResponse.getError())) {
                showError(R.string.generic_error);
            } else {
                showError(errorResponse.getError());
            }
        }
        hideLoadingView();
    }

    protected void showError(Response response, View.OnClickListener retryClick) {
        if (isAdded()) {
            ErrorResponse errorResponse = ErrorUtils.parseError(response);
            if (errorResponse != null && !TextUtils.isEmpty(errorResponse.getError())) {
                showError(retryClick);
            } else {
                showError(errorResponse.getError(), retryClick);
            }
        }
        hideLoadingView();
    }

    protected void showError(Throwable t) {
        if (!isAdded()) {
            return;
        }
        showError(R.string.generic_error);
    }

    protected void showError(Throwable t, View.OnClickListener retryClick) {
        if (!isAdded()) {
            return;
        }
        showError(retryClick);
    }

    protected void showError(String msg) {
        if (!isAdded()) {
            return;
        }
        showError(msg, null, null, null, null);
    }

    protected void showError(int msg) {
        if (!isAdded()) {
            return;
        }
        showError(getString(msg));
    }

    protected void showError(int msg, View.OnClickListener positiveButtonClick, String positiveButton) {
        if (!isAdded()) {
            return;
        }
        showError(msg, positiveButtonClick, null, positiveButton, null);
    }

    protected void showError(int msg, View.OnClickListener positiveButtonClick, View.OnClickListener negativeButtonClick, String positiveButton, String negativeButton) {
        if (!isAdded()) {
            return;
        }
        showError(getString(msg), positiveButtonClick, negativeButtonClick, positiveButton, negativeButton);
    }

    protected void showError(String msg, View.OnClickListener positiveButtonClick, View.OnClickListener negativeButtonClick, String positiveButton, String negativeButton) {
        if (!isAdded()) {
            return;
        }
        if (!showCustomError(msg, positiveButtonClick, negativeButtonClick, positiveButton, negativeButton)) {
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        }
        hideLoadingView();
    }

    protected void showDialog(String msg, final View.OnClickListener positiveButtonClick, final View.OnClickListener negativeButtonClick, String positiveButton, String negativeButton) {
        if (!isAdded()) {
            return;
        }
        showCustomError(msg, positiveButtonClick, negativeButtonClick, positiveButton, negativeButton);
    }

    private boolean showCustomError(String msg, final View.OnClickListener positiveButtonClick, final View.OnClickListener negativeButtonClick, String positiveButton, String negativeButton) {
        if (isAdded() && getCustomError() != null && getLblError() != null) {

            getLblError().setText(msg);
            if (getPositiveErrorButton() != null) {
                if (!TextUtils.isEmpty(positiveButton)) {
                    getPositiveErrorButton().setText(positiveButton);
                    if (positiveButtonClick != null) {
                        getPositiveErrorButton().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                closeError(100);
                                positiveButtonClick.onClick(v);
                            }
                        });
                    } else {
                        getPositiveErrorButton().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                closeError(100);
                            }
                        });
                    }
                    getCustomError().findViewById(R.id.layout_buttons).setVisibility(View.VISIBLE);
                    getPositiveErrorButton().setVisibility(View.VISIBLE);
                } else {
                    getPositiveErrorButton().setVisibility(View.GONE);
                }
            }

            if (getNegativeErrorButton() != null) {
                if (!TextUtils.isEmpty(negativeButton)) {
                    getNegativeErrorButton().setText(negativeButton);
                    if (negativeButtonClick != null) {
                        getNegativeErrorButton().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                closeError(100);
                                negativeButtonClick.onClick(v);
                            }
                        });
                    } else {
                        getNegativeErrorButton().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                closeError(100);
                            }
                        });
                    }
                    getCustomError().findViewById(R.id.layout_buttons).setVisibility(View.VISIBLE);
                    getNegativeErrorButton().setVisibility(View.VISIBLE);
                } else {
                    getNegativeErrorButton().setVisibility(View.GONE);
                }
            }

            getCustomError().measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            getCustomError().setTranslationY(-getCustomError().getMeasuredHeight());
            getCustomError().setVisibility(View.VISIBLE);
            getCustomError().bringToFront();

            if (TextUtils.isEmpty(positiveButton) && TextUtils.isEmpty(negativeButton)) {
                ViewCompat.animate(getCustomError()).setStartDelay(Constants.DELAY_START_ANIMATION_SNACK).translationY(0).setDuration(Constants.ANIMATION_DURATION_SNACK).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        closeError(Constants.TIME_SHOW_MSG_SNACK);
                    }
                });
            } else {
                ViewCompat.animate(getCustomError()).setStartDelay(Constants.DELAY_START_ANIMATION_SNACK).translationY(0).setDuration(Constants.ANIMATION_DURATION_SNACK);
            }

            return true;
        } else {
            return false;
        }
    }

    private void closeError(final int delay) {
        if (!isAdded()) {
            return;
        }
        if (isAdded()) {
            ViewCompat.animate(getCustomError()).setStartDelay(delay).translationY(-getCustomError().getMeasuredHeight()).setDuration(Constants.ANIMATION_DURATION_SNACK);
        }
    }

    protected View.OnClickListener cancelRetryClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    protected boolean backButtonNavigateToPreviousFragment() {
        return true;
    }

    protected abstract View getCustomError();

    protected abstract TextView getLblError();

    protected abstract TextView getPositiveErrorButton();

    protected abstract TextView getNegativeErrorButton();
}
