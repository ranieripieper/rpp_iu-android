package com.doisdoissete.iu.library.service.interfaces;

import com.doisdoissete.iu.library.model.Teacher;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by ranipieper on 3/30/16.
 */
public interface TeacherService {

    @POST("users")
    Call<Teacher> signup(@Body Teacher user);

    @POST("requestPasswordReset")
    Call<Void> requestPasswordReset(@Body Teacher user);

    @GET("login")
    Call<Teacher> login(@Query("username") String email, @Query("password") String password);
}
