package com.doisdoissete.iu.library.service.interfaces;

import com.doisdoissete.iu.library.model.entry.AddStudentEntry;
import com.doisdoissete.iu.library.model.entry.StudentServiceEntry;
import com.doisdoissete.iu.library.model.response.LogoutResponse;
import com.doisdoissete.iu.library.model.response.StudentResponse;
import com.doisdoissete.iu.library.model.response.StudentsResponse;
import com.doisdoissete.iu.library.model.response.SummaryResponse;
import com.doisdoissete.iu.library.model.response.TeachersResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ranipieper on 3/17/16.
 */
public interface StudentService {

    String DEFAULT_ORDER = "name";

    @GET("classes/Student")
    Call<StudentsResponse> getStudent(@Query("where") String where);

    @GET("classes/Student")
    Call<StudentsResponse> getStudent(@Query("where") String where, @Query("order") String order);

    @GET("classes/Student")
    Call<StudentsResponse> addStudent(@Query("where") String where);

    @DELETE("classes/Student/{id}")
    Call<Void> removeStudent(@Path("id") String objectId);

    @POST("functions/student_get_teachers")
    Call<TeachersResponse> getTeachers(@Body StudentServiceEntry params);

    @POST("functions/student_get_summary")
    Call<SummaryResponse> getSummary(@Body StudentServiceEntry params);

    @POST("functions/students_add")
    Call<StudentResponse> addStudent(@Body AddStudentEntry params);


    @POST("functions/student_logout")
    Call<LogoutResponse> logout(@Body StudentServiceEntry params);

}
