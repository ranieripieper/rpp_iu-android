package com.doisdoissete.iu.library.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by ranipieper on 3/24/16.
 */
public class DateParse {

    @Expose
    @SerializedName("__type")
    private String type = "Date";

    @Expose
    @SerializedName("iso")
    private Date date;

    public DateParse() {
        type = "Date";
    }

    public DateParse(Date date) {
        this.date = date;
        type = "Date";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
