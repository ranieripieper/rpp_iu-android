package com.doisdoissete.iu.library.model.parse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 3/31/16.
 */
public class ParseRelationEntry {

    ////instructors":{"__op":"AddRelation","objects":[{"__type":"Pointer","className":"_User","objectId":"WJm4PGIyKi"}]}}

    @Expose
    @SerializedName("__op")
    private String op;

    @Expose
    @SerializedName("objects")
    private List<RelationshipParse> objects;

    public ParseRelationEntry(String op, List<RelationshipParse> objects) {
        this.op = op;
        this.objects = objects;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public List<RelationshipParse> getObjects() {
        return objects;
    }

    public void setObjects(List<RelationshipParse> objects) {
        this.objects = objects;
    }
}
