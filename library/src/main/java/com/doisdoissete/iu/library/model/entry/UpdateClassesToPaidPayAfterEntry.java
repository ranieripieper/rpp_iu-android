package com.doisdoissete.iu.library.model.entry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 4/27/16.
 */
public class UpdateClassesToPaidPayAfterEntry {

    @Expose
    @SerializedName("teacher_id")
    private String teacherId;

    @Expose
    @SerializedName("student_id")
    private String studentId;

    @Expose
    @SerializedName("pay_at")
    private String payAt;

    @Expose
    @SerializedName("qt_classes")
    private Long qtClasses;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public Long getQtClasses() {
        return qtClasses;
    }

    public void setQtClasses(Long qtClasses) {
        this.qtClasses = qtClasses;
    }

    /**
     * Gets the payAt
     *
     * @return payAt
     */
    public String getPayAt() {
        return payAt;
    }

    /**
     * Sets the payAt
     *
     * @param payAt
     */
    public void setPayAt(String payAt) {
        this.payAt = payAt;
    }
}
