package com.doisdoissete.iu.library.helper;

import android.text.TextUtils;

import com.doisdoissete.iu.library.model.Clazz;
import com.doisdoissete.iu.library.model.MonthClazz;
import com.doisdoissete.iu.library.util.DateUtil;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ranipieper on 3/24/16.
 */
public class ClassHelper {

    public static String getDatas(List<Clazz> classes) {
        String datas = "";
        String separator = "  ";
        for (Clazz clazz : classes) {
            Date dt = clazz.getCreatedAt();
            if (clazz.getUsedAt() != null && clazz.getUsedAt().getDate() != null) {
                dt = clazz.getUsedAt().getDate();
            }
            if (dt != null) {
                datas += String.format("%s%s", DateUtil.DATE_DAY_MONTH_YEAR.get().format(dt), separator);
            }
        }
        if (!TextUtils.isEmpty(datas)) {
            datas = datas.substring(0, datas.length() - separator.length());
        }
        return datas;
    }

    public static String getMonth(String yearMonth) {
        try {
            return DateUtil.DATE_MONTH_YEAR.get().format(DateUtil.DATE_YEAR_MONTH.get().parse(yearMonth));
        } catch (ParseException e) {
            return yearMonth;
        }
    }

    public static List<MonthClazz> getMonthClazz(List<Clazz> classes) {
        Map<String, List<Clazz>> monthClazzMap = new HashMap();
        if (classes == null || classes.isEmpty()) {
            return new ArrayList<>();
        }

        List<MonthClazz> result = new ArrayList<>();
        for (Clazz clazz : classes) {
            Date date = clazz.getCreatedAt();
            if (clazz.getUsedAt() != null && clazz.getUsedAt().getDate() != null) {
                date = clazz.getUsedAt().getDate();
            }
            String yearMonth = DateUtil.DATE_YEAR_MONTH.get().format(date);
            List<Clazz> lst = monthClazzMap.get(yearMonth);
            if (lst == null) {
                lst = new ArrayList<>();
            }
            lst.add(clazz);
            monthClazzMap.put(yearMonth, lst);
        }

        for (String key : monthClazzMap.keySet()) {
            MonthClazz monthClazz = new MonthClazz();
            monthClazz.setYearMonth(key);
            monthClazz.setClasses(monthClazzMap.get(key));
            result.add(monthClazz);
        }

        Collections.sort(result);

        return result;
    }
}
