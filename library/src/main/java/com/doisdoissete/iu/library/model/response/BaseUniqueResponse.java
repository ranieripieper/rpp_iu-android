package com.doisdoissete.iu.library.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 1/14/16.
 */
public class BaseUniqueResponse<T> {

    @Expose
    @SerializedName("result")
    private T result;

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
