package com.doisdoissete.iu.library.model.response;

import com.doisdoissete.iu.library.model.Teacher;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 3/17/16.
 */
public class TeachersResponse {

    @Expose
    @SerializedName("result")
    private List<Teacher> results;

    public boolean isEmpty() {
        if (results == null || results.isEmpty()) {
            return true;
        }
        return false;
    }

    public List<Teacher> getResults() {
        return results;
    }

    public void setResults(List<Teacher> results) {
        this.results = results;
    }
}
