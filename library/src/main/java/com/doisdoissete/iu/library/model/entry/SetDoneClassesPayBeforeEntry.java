package com.doisdoissete.iu.library.model.entry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 4/27/16.
 */
public class SetDoneClassesPayBeforeEntry {

    @Expose
    @SerializedName("teacher_id")
    private String teacherId;

    @Expose
    @SerializedName("student_id")
    private String studentId;

    @Expose
    @SerializedName("expired_at")
    private String expiredAt;

    @Expose
    @SerializedName("dates")
    private List<String> dates;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public List<String> getDates() {
        return dates;
    }

    public void setDates(List<String> dates) {
        this.dates = dates;
    }

    public String getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(String expiredAt) {
        this.expiredAt = expiredAt;
    }
}
