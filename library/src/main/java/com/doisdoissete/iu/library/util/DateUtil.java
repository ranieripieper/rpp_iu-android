package com.doisdoissete.iu.library.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ranipieper on 11/12/15.
 */
public class DateUtil {

    public static final ThreadLocal<DateFormat> DATE_MONTH_YEAR = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("MMMM yyyy");
        }
    };

    public static final ThreadLocal<DateFormat> DATE_YEAR_MONTH = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM");
        }
    };

    public static final ThreadLocal<DateFormat> DATE_YEAR_MONTH_DAY = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };

    public static final ThreadLocal<DateFormat> DATE_DAY_MONTH_YEAR = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("dd.MM.yyyy");
        }
    };


    public static int getDifInDays(Date dt) {
        if (dt == null) {
            return 0;
        }
        long diferenca = new Date().getTime() - dt.getTime();
        Double diferencaEmDias = Double.valueOf((diferenca /1000) / 60 / 60 /24); //resultado é diferença entre as datas em dias
        return diferencaEmDias.intValue();
    }

}
