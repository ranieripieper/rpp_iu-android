package com.doisdoissete.iu.library.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by ranipieper on 4/26/16.
 */
public class Summary {

    @Expose
    @SerializedName("student_name")
    private String studentName;

    @Expose
    @SerializedName("qt_classes")
    private long qtClasses;

    @Expose
    @SerializedName("recent_classes")
    private List<Clazz> recentClasses;

    @Expose
    @SerializedName("type")
    private Integer type;

    @Expose
    @SerializedName("date")
    private DateParse date;

    public long getQtClasses() {
        return qtClasses;
    }

    public void setQtClasses(long qtClasses) {
        this.qtClasses = qtClasses;
    }

    public List<Clazz> getRecentClasses() {
        return recentClasses;
    }

    public void setRecentClasses(List<Clazz> recentClasses) {
        this.recentClasses = recentClasses;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public DateParse getDateParse() {
        return date;
    }

    public void setDate(DateParse date) {
        this.date = date;
    }

    public Date getDate() {
        if (date != null) {
            return date.getDate();
        }
        return null;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
}

