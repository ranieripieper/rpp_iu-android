package com.doisdoissete.iu.library.model.parse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 4/1/16.
 */
public class ParseBatchEntry {

    @Expose
    @SerializedName("method")
    private String method;

    @Expose
    @SerializedName("path")
    private String path;

    @Expose
    @SerializedName("body")
    private Object body;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }
}
