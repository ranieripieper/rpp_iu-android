package com.doisdoissete.iu.library.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 7/29/16.
 */
public class PushMessage {

    //{"alert":"Olá Rani, Rani acabou de marcar uma aula feita sua no dia 27 de Julho. Você já fez 3 aula.\ninfo us","parsePushId":"EU9LlkykJM","push_hash":"313bc532d3abec466329e8b755bb2d15"}

    @Expose
    @SerializedName("alert")
    private String alert;

    @Expose
    @SerializedName("parsePushId")
    private String parsePushId;

    @Expose
    @SerializedName("push_hash")
    private String pushHash;

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public String getParsePushId() {
        return parsePushId;
    }

    public void setParsePushId(String parsePushId) {
        this.parsePushId = parsePushId;
    }

    public String getPushHash() {
        return pushHash;
    }

    public void setPushHash(String pushHash) {
        this.pushHash = pushHash;
    }
}
