package com.doisdoissete.iu.library.util;

import android.content.Context;

/**
 * Created by ranipieper on 6/7/16.
 */
public class ResourceUtil {

    public static int getStringId(String name, Context context) {
        return context.getResources().getIdentifier(name, "string",
                context.getPackageName());
    }
}
