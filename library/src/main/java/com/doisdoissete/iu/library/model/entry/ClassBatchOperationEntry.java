package com.doisdoissete.iu.library.model.entry;

import com.doisdoissete.iu.library.model.parse.ParseBatchEntry;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 4/1/16.
 */
public class ClassBatchOperationEntry {

    @Expose
    @SerializedName("requests")
    private List<ParseBatchEntry> requests;

    public List<ParseBatchEntry> getRequests() {
        return requests;
    }

    public void setRequests(List<ParseBatchEntry> requests) {
        this.requests = requests;
    }
}
