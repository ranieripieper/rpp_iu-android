package com.doisdoissete.iu.library.view.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by ranipieper on 4/28/16.
 */
public class TouchBlackHoleView extends View {

    public TouchBlackHoleView(Context context) {
        super(context);
    }

    public TouchBlackHoleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TouchBlackHoleView(Context context, AttributeSet attrs,
                              int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private boolean mTouchDisabled = true;

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        return mTouchDisabled;
    }

    public void disableTouch(boolean b) {
        mTouchDisabled = b;
    }
}