package com.doisdoissete.iu.library.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.doisdoissete.iu.library.model.Config;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.util.Date;

public class SharedPrefManager {

    private static SharedPrefManager sharedPrefManager;
    private Context mContext;

    private static final String PREFERENCES = SharedPrefManager.class + "";

    private static final String CONFIG = "CONFIG";
    private static final String VERIFY_UPDATE = "VERIFY_UPDATE";

    private SharedPrefManager(Context context) {
        this.mContext = context;
    }

    public static SharedPrefManager getInstance() {
        return sharedPrefManager;
    }

    public static void init(Application application) {
        sharedPrefManager = new SharedPrefManager(application);
    }

    private SharedPreferences getSharedPreferences() {
        return mContext.getSharedPreferences(PREFERENCES, 0);
    }

    public Config getConfig() {
        String json = getSharedPreferences().getString(CONFIG, null);
        try {
            if (!TextUtils.isEmpty(json)) {
                return getGson().fromJson(json, Config.class);
            }
        } catch (JsonSyntaxException e) {
        }
        return null;
    }

    public void setConfig(Config value) {
        if (value != null) {
            SharedPreferences.Editor editor = getEditor();

            if (value.getConfigIu() != null && value.getConfigIu().getDtGetServer() == null) {
                value.getConfigIu().setDtGetServer(new Date());
            }
            if (value.getConfigIuPro() != null && value.getConfigIuPro().getDtGetServer() == null) {
                value.getConfigIuPro().setDtGetServer(new Date());
            }
            editor.putString(CONFIG, getGson().toJson(value));
            editor.commit();
        }

    }

    public boolean verifyUpdate() {
        return getSharedPreferences().getBoolean(VERIFY_UPDATE, true);
    }

    public void setVerifyUpdate(boolean verifyUpdate) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(VERIFY_UPDATE, verifyUpdate);
        editor.commit();
    }

    private Gson getGson() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();
        return gson;
    }

    private SharedPreferences.Editor getEditor() {
        return getSharedPreferences().edit();
    }

}
