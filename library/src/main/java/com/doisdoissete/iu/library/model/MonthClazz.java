package com.doisdoissete.iu.library.model;

import android.text.TextUtils;

import com.doisdoissete.iu.library.util.DateUtil;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by ranipieper on 3/24/16.
 */
public class MonthClazz implements Comparable<MonthClazz> {

    private String yearMonth;
    private List<Clazz> classes;

    @Override
    public int compareTo(MonthClazz another) {
        try {
            if (!TextUtils.isEmpty(getYearMonth()) && !TextUtils.isEmpty(another.getYearMonth())) {
                Date dtLhs = DateUtil.DATE_YEAR_MONTH.get().parse(getYearMonth());
                Date dtRhs = DateUtil.DATE_YEAR_MONTH.get().parse(another.getYearMonth());
                return dtLhs.compareTo(dtRhs);
            }
        } catch (ParseException e) {

        }

        return 0;
    }

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }

    public List<Clazz> getClasses() {
        return classes;
    }

    public void setClasses(List<Clazz> classes) {
        this.classes = classes;
    }
}
