package com.doisdoissete.iu.cache;

import com.doisdoissete.iu.cache.base.BaseCache;
import com.doisdoissete.iu.cache.model.StudentCache;
import com.doisdoissete.iu.library.model.Student;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by ranipieper on 7/25/16.
 */
public class StudentCacheManager extends BaseCache<StudentCache> {

    private static StudentCacheManager instance = new StudentCacheManager();

    private StudentCacheManager() {
    }

    public static StudentCacheManager getInstance() {
        return instance;
    }

    @Override
    public String getPrimaryKeyName() {
        return "objectId";
    }

    public void updateWithoutNullsStudent(Realm realm, Student obj) {
        if (obj != null) {
            super.updateWithoutNullsAllFields(realm, new StudentCache(obj));
        }
    }

    public void updateWithoutNullsStudent(Realm realm, List<Student> lst) {
        if (lst != null) {
            List<StudentCache> lstCache = new ArrayList<>();
            for (Student obj : lst) {
                lstCache.add(new StudentCache(obj));
            }
            super.updateWithoutNullsAllFields(realm, lstCache);
        }
    }

    @Override
    public Class<StudentCache> getReferenceClass() {
        return StudentCache.class;
    }
}