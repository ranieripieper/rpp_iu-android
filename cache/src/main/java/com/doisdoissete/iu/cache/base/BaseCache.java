package com.doisdoissete.iu.cache.base;

import com.doisdoissete.iu.library.util.DateUtil;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by ranipieper on 7/25/16.
 */
public abstract class BaseCache<T extends RealmObject> implements Cache<T>{

    public BaseCache() {
    }

    private void updateWithoutNulls(Realm realm, T obj, Gson gson) {
        if (obj != null) {
            realm.beginTransaction();
            String json = gson.toJson(obj);
            realm.createOrUpdateObjectFromJson(getReferenceClass(), json);
            realm.commitTransaction();
        }
    }

    private void updateWithoutNulls(Realm realm, List<T> lst, Gson gson) {
        if (lst != null) {
            realm.beginTransaction();
            String json = gson.toJson(lst);
            realm.createOrUpdateAllFromJson(getReferenceClass(), json);
            realm.commitTransaction();
        }
    }

    @Override
    public void updateWithoutNullsAllFields(Realm realm, List<T> lst) {
        updateWithoutNulls(realm, lst, getGsonRealm(true));
    }

    @Override
    public void updateWithoutNullsAllFields(Realm realm, T obj) {
        updateWithoutNulls(realm, obj, getGsonRealm(true));
    }

    @Override
    public void updateWithoutNullsOnlyExposeGson(Realm realm, List<T> lst) {
        updateWithoutNulls(realm, lst, getGsonExpose(true));
    }

    @Override
    public void updateWithoutNullsOnlyExposeGson(Realm realm, T obj) {
        updateWithoutNulls(realm, obj, getGsonExpose(true));
    }

    public List<T> getAll(Realm realm, String sortField, boolean sortAscending) {
        return getAll(realm, sortField, sortAscending ? Sort.ASCENDING : Sort.DESCENDING);
    }

    public List<T> getAll(Realm realm, String sortField, Sort sort) {
        RealmResults allRealmObjects = realm.where(getReferenceClass()).findAllSorted(sortField, sort);
        return allRealmObjects;
    }

    public List<T> getAll(Realm realm) {
        RealmResults allRealmObjects = realm.where(getReferenceClass()).findAll();
        return allRealmObjects;
    }

    @Override
    public T put(Realm realm, T t) {
        realm.setAutoRefresh(true);
        realm.beginTransaction();
        T result = realm.copyToRealmOrUpdate(t);
        realm.commitTransaction();
        return copyFromRealm(realm, result);
    }

    @Override
    public List<T> putAll(Realm realm, List<T> t) {
        realm.setAutoRefresh(true);
        realm.beginTransaction();

        List<T> result = new ArrayList<>();
        for (RealmObject item : t) {
            result.add((T)realm.copyToRealmOrUpdate(item));
        }

        realm.commitTransaction();
        return result;
    }

    @Override
    public void delete(Realm realm, final Long id) {
        realm.setAutoRefresh(true);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Class<T> type = getReferenceClass();
                T obj = realm.where(type).equalTo(getPrimaryKeyName(), id).findFirst();
                if (obj != null) {
                    obj.deleteFromRealm();
                }
            }
        });
    }

    @Override
    public void delete(Realm realm, final String id) {
        realm.setAutoRefresh(true);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Class<T> type = getReferenceClass();
                T obj = realm.where(type).equalTo(getPrimaryKeyName(), id).findFirst();
                if (obj != null) {
                    obj.deleteFromRealm();
                }
            }
        });
    }

    @Override
    public T get(Realm realm, String id) {
        Class<T> type = getReferenceClass();
        return copyFromRealm(realm, realm.where(type).equalTo(getPrimaryKeyName(), id).findFirst());
    }

    @Override
    public T get(Realm realm, Long id) {
        return copyFromRealm(realm, realm.where(getReferenceClass()).equalTo(getPrimaryKeyName(), id).findFirst());
    }

    protected T copyFromRealm(Realm realm, T obj) {
        if (obj == null) {
            return null;
        }
        return realm.copyFromRealm(obj);
    }

    @Override
    public void deleteAll(Realm realm) {
        realm.beginTransaction();
        realm.where(getReferenceClass()).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }

    @Override
    public long count(Realm realm) {
        return realm.where(getReferenceClass()).count();
    }

    @Override
    public String getPrimaryKeyName() {
        return "id";
    }

    public abstract Class<T> getReferenceClass();


    private static Gson getGsonRealm(boolean toUpdate, boolean onlyExpose) {
        GsonBuilder builder = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .registerTypeAdapter(Date.class, dateDeserializer)
                .registerTypeAdapter(Date.class, dateSerializer);
        if (!toUpdate) {
            builder.setFieldNamingStrategy(new FieldNamingStrategy() {
                @Override
                public String translateName(Field field) {
                    if (field.isAnnotationPresent(SerializedGsonName.class)) {
                        SerializedGsonName serializedName = field.getAnnotation(SerializedGsonName.class);
                        return serializedName.value();
                    }
                    return field.getName();
                }
            });
        }
        if (onlyExpose) {
            builder.excludeFieldsWithoutExposeAnnotation();
        }

        return builder.create();
    }

    public static Gson getGsonRealm(boolean toUpdate) {
        return getGsonRealm(toUpdate, false);
    }

    public static Gson getGsonExpose(boolean toUpdate) {
        return getGsonRealm(toUpdate, true);
    }



    static JsonDeserializer<Date> dateDeserializer = new JsonDeserializer<Date>() {
        @Override
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            if (json != null) {
                try {
                    Date dt = DATE_SERVICE_1.get().parse(json.getAsString());
                    if (dt == null) {
                        return tryOtherFormat(json);
                    }
                    return dt;
                } catch (ParseException e) {
                    return tryOtherFormat(json);
                }
            }

            return null;
        }

        private Date tryOtherFormat(JsonElement json) {
            try {
                return DATE_SERVICE_2.get().parse(json.getAsString());
            } catch (ParseException e1) {
                return tryOtherFormat2(json);
            }
        }

        private Date tryOtherFormat2(JsonElement json) {
            try {
                return DateUtil.DATE_YEAR_MONTH_DAY.get().parse(json.getAsString());
            } catch (ParseException e1) {
            }
            return null;
        }
    };


    static JsonSerializer<Date> dateSerializer = new JsonSerializer<Date>() {
        @Override
        public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext
                context) {
            return src == null ? null : new JsonPrimitive(src.getTime());
        }
    };

    private static final ThreadLocal<DateFormat> DATE_SERVICE_1 = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ") {
                public StringBuffer format(Date date, StringBuffer toAppendTo, java.text.FieldPosition pos) {
                    StringBuffer toFix = super.format(date, toAppendTo, pos);
                    return toFix.insert(toFix.length()-2, ':');
                };

                public Date parse(String text, ParsePosition pos) {
                    int indexOf = text.indexOf(':', text.length() - 4);
                    if (indexOf > 0) {
                        text = text.substring(0, indexOf) + text.substring(indexOf+1, text.length());
                        return super.parse(text, pos);
                    } else {
                        return null;
                    }

                }

            };
        }
    };

    private static final ThreadLocal<DateFormat> DATE_SERVICE_2 = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        }
    };

}