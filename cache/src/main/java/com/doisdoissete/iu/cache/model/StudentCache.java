package com.doisdoissete.iu.cache.model;

import com.doisdoissete.iu.cache.base.SerializedGsonName;
import com.doisdoissete.iu.library.model.Student;
import com.google.gson.annotations.Expose;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranipieper on 8/10/16.
 */
public class StudentCache extends RealmObject {

    @Expose
    @SerializedGsonName("objectId")
    @PrimaryKey
    private String objectId;

    @Expose
    @SerializedGsonName("name")
    private String name;

    @Expose
    @SerializedGsonName("phone")
    private String phone;

    @Expose
    @SerializedGsonName("createdAt")
    private Date createdAt;

    @Expose
    @SerializedGsonName("updatedAt")
    private Date updatedAt;

    @Expose
    @SerializedGsonName("invited")
    private Boolean invited;

    public StudentCache() {

    }

    public StudentCache(Student student) {
        this.objectId = student.getObjectId();
        this.name = student.getName();
        this.phone = student.getPhone();
        this.createdAt = student.getCreatedAt();
        this.updatedAt = student.getUpdatedAt();
    }

    public Student toStudent() {

        Student student = new Student();
        student.setObjectId(this.objectId);
        student.setName(this.name);
        student.setPhone(this.phone);
        student.setCreatedAt(this.createdAt);
        student.setUpdatedAt(this.updatedAt);
        return student;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isInvited() {
        if (invited != null) {
            return invited;
        }
        return false;
    }

    public void setInvited(Boolean invited) {
        this.invited = invited;
    }
}
