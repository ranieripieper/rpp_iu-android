package com.doisdoissete.iu.cache.base;

import android.content.Context;

import com.doisdoissete.iu.cache.BuildConfig;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by ranipieper on 7/25/16.
 */
public class RealmManager {

    private static final long REALMVERSION = 1;
    private static RealmConfiguration mConfig;

    private RealmManager() {
    }

    public static void init(final Context context, String appName) {
        RealmConfiguration.Builder builder = new RealmConfiguration.Builder(context)
                .schemaVersion(REALMVERSION)
                .name(String.format("%s.db", appName));

        if (BuildConfig.DEBUG) {
            builder.deleteRealmIfMigrationNeeded();
        }
        mConfig = builder.build();
        Realm.setDefaultConfiguration(mConfig);
    }

}
