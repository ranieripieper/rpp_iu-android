package com.doisdoissete.iu;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.CrashlyticsCore;
import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsSession;
import com.doisdoissete.iu.library.IuAppApplicationBase;
import com.doisdoissete.iu.library.util.Constants;
import com.doisdoissete.iu.util.SharedPrefManager;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;

import io.fabric.sdk.android.Fabric;

public class IuAppApplication extends IuAppApplicationBase {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "jNQZf3UxHScHx0970f9cVJk3Z";
    private static final String TWITTER_SECRET = "VeqOqZIWua4ym3WYMIDUQO8iQdK8lSaUK7MsUjh82Gjj4fossz";


    AuthCallback authCallback;

    @Override
    public void onCreate() {
        super.onCreate();

        SharedPrefManager.init(this);

    }

    protected void initCrashlytics() {
        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder()
                        .disabled(com.doisdoissete.iu.library.BuildConfig.DEBUG)
                        .build())
                .build();
        // Initialize Fabric with the debug-disabled crashlytics.

        TwitterAuthConfig authConfig =  new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        //Fabric.with(this, new TwitterCore(authConfig), new Digits());

        if (!com.doisdoissete.iu.library.BuildConfig.DEBUG) {
            Fabric.with(this, crashlyticsKit, new Answers(), new Crashlytics(), new TwitterCore(authConfig), new Digits());
        } else {
            Fabric.with(this, crashlyticsKit, new Answers(), new TwitterCore(authConfig), new Digits());
        }

        authCallback = new AuthCallback() {
            @Override
            public void success(DigitsSession session, String phoneNumber) {
                Log.e("TAG", phoneNumber + "");
            }

            @Override
            public void failure(DigitsException exception) {
                Log.e("TAG", exception.getMessage() + "");
            }
        };

    }

    public AuthCallback getAuthCallback(){
        return authCallback;
    }

    @Override
    public Integer getVersionCode() {
        return BuildConfig.VERSION_CODE;
    }

    @Override
    public int getAppId() {
        return Constants.IU_APP_ID;
    }

}
