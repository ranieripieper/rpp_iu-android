package com.doisdoissete.iu;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.doisdoissete.iu.library.model.PushMessage;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.doisdoissete.iu.view.SplashScreenActivity;
import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;

import java.util.Date;
import java.util.Random;


/**
 * Created by ranipieper on 12/10/15.
 */
public class ReceiverPush extends ParsePushBroadcastReceiver {

    private static final String TAG = "ReceiverPush";

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        try {
            String data = intent.getStringExtra("com.parse.Data");
            PushMessage pushMessage = RetrofitManager.getGson().fromJson(data, PushMessage.class);

            if (pushMessage != null && !TextUtils.isEmpty(pushMessage.getAlert())) {
                showNotification(context, intent, pushMessage.getAlert());
            }
        } catch (Exception e) {
            Log.e(ReceiverPush.class.toString(), "Unexpected JSONException when receiving push data: ", e);
        }
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        ParseAnalytics.trackAppOpenedInBackground(intent);

        Class<? extends Activity> cls = getActivity(context, intent);
        Intent activityIntent = SplashScreenActivity.getActivityIntent(context);
        if (intent != null) {
            activityIntent.putExtras(intent.getExtras());
        }
        if (Build.VERSION.SDK_INT >= 16) {
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(cls);
            stackBuilder.addNextIntent(activityIntent);
            stackBuilder.startActivities();
        } else {
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(activityIntent);
        }
    }

    public static void showNotification(Context context, final Intent intent, String message) {
        showNotification(context, intent, message, R.drawable.ic_stat_iu);
    }

    public static void showNotification(Context context, final Intent intent, String message, int icon) {

        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_calendar);//getCircularBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_calendar));

        Bundle extras = new Bundle();
        if (intent != null) {
            extras = intent.getExtras();
        }

        Random random = new Random();
        int contentIntentRequestCode = random.nextInt();
        int deleteIntentRequestCode = random.nextInt();
        String packageName = context.getPackageName();
        Intent contentIntent = new Intent("com.parse.push.intent.OPEN");
        contentIntent.putExtras(extras);
        contentIntent.setPackage(packageName);
        Intent deleteIntent = new Intent("com.parse.push.intent.DELETE");
        deleteIntent.putExtras(extras);
        deleteIntent.setPackage(packageName);
        PendingIntent pContentIntent = PendingIntent.getBroadcast(context, contentIntentRequestCode, contentIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent pDeleteIntent = PendingIntent.getBroadcast(context, deleteIntentRequestCode, deleteIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        android.support.v4.app.NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(icon)
                        .setContentTitle(context.getString(R.string.APP_NAME))
                        .setContentText(message)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setAutoCancel(true)
                        .setCategory(NotificationCompat.CATEGORY_SOCIAL)
                        .setGroup(context.getString(R.string.APP_NAME))
                        .setContentIntent(pContentIntent)
                        .setDeleteIntent(pDeleteIntent)
                        .setColor(context.getResources().getColor(R.color.bgNotification))
                        .setLargeIcon(largeIcon);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        mBuilder.setDefaults(Notification.DEFAULT_ALL);

        Long id = new Date().getTime();
        notificationManager.notify(Long.valueOf(id).intValue(), mBuilder.build());
    }

    public static Bitmap getCircularBitmap(Bitmap bitmap) {
        Bitmap output;

        if (bitmap != null) {
            if (bitmap.getWidth() > bitmap.getHeight()) {
                output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            } else {
                output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

            float r = 0;

            if (bitmap.getWidth() > bitmap.getHeight()) {
                r = bitmap.getHeight() / 2;
            } else {
                r = bitmap.getWidth() / 2;
            }

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawCircle(r, r, r, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            return output;
        }

        return null;

    }
}