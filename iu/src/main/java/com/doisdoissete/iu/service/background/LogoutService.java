package com.doisdoissete.iu.service.background;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.text.TextUtils;

import com.doisdoissete.iu.library.model.entry.StudentServiceEntry;
import com.doisdoissete.iu.library.model.response.LogoutResponse;
import com.doisdoissete.iu.library.service.RetrofitManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranieripieper on 3/25/16.
 */
public class LogoutService extends Service {

    public static String EXTRA_PHONE_NUMBER = "EXTRA_PHONE_NUMBER";
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        final String phoneNumber = intent.getStringExtra(EXTRA_PHONE_NUMBER);

        new Thread(new Runnable() {
            @Override
            public void run() {
                backgroundTask(phoneNumber);
            }
        }).start();

        return START_NOT_STICKY;
    }


    private void backgroundTask(final String phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            StudentServiceEntry entry = new StudentServiceEntry();
            entry.setPhoneNumber(phoneNumber);
            Call<LogoutResponse> service = RetrofitManager.getInstance().getStudentService().logout(entry);
            service.enqueue(new Callback<LogoutResponse>() {
                @Override
                public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                }

                @Override
                public void onFailure(Call<LogoutResponse> call, Throwable t) {
                }
            });
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}