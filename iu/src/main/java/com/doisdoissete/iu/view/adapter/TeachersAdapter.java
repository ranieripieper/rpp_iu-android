package com.doisdoissete.iu.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.doisdoissete.iu.R;
import com.doisdoissete.iu.library.model.Teacher;

import java.util.List;

/**
 * Created by ranipieper on 3/24/16.
 */
public class TeachersAdapter extends RecyclerView.Adapter<TeachersAdapter.ViewHolder> {
    private List<Teacher> mDataset;
    private TeacherAdapterListener mListener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mLblInfo;
        public TextView mLblTeacher;
        public View mLayoutRow;

        public ViewHolder(View v) {
            super(v);
            mLblInfo = (TextView) v.findViewById(R.id.lbl_info);
            mLblTeacher = (TextView) v.findViewById(R.id.lbl_teacher);
            mLayoutRow = v.findViewById(R.id.layout_row);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public TeachersAdapter(List<Teacher> myDataset, TeacherAdapterListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TeachersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_teacher, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Teacher teacher = mDataset.get(position);

        holder.mLblTeacher.setText(teacher.getName());

        holder.mLayoutRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onTeacherClick(teacher, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void addData(List data) {
        if (null == data || data.isEmpty()) {
            return;
        }

        int startPosition = getItemCount();
        mDataset.addAll(data);

        notifyItemRangeInserted(startPosition, getItemCount());
    }

    public interface TeacherAdapterListener {
        void onTeacherClick(Teacher teacher, int position);
    }
}