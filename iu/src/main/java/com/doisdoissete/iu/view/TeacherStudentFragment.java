package com.doisdoissete.iu.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.doisdoissete.iu.R;
import com.doisdoissete.iu.library.helper.ClassHelper;
import com.doisdoissete.iu.library.model.Student;
import com.doisdoissete.iu.library.model.Summary;
import com.doisdoissete.iu.library.model.Teacher;
import com.doisdoissete.iu.library.model.entry.StudentServiceEntry;
import com.doisdoissete.iu.library.model.response.SummaryResponse;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.doisdoissete.iu.library.util.Constants;
import com.doisdoissete.iu.library.util.DateUtil;
import com.doisdoissete.iu.util.SharedPrefManager;
import com.doisdoissete.iu.view.base.BaseFragment;
import com.doisdoissete.iu.view.base.BaseFragmentActivity;
import com.doisdoissete.iu.view.navigation.Navigator;

import java.util.Date;

import butterknife.Bind;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 3/17/16.
 */
public class TeacherStudentFragment extends BaseFragment {

    @Bind(R.id.layout_aulas_futuras)
    View mLayoutAulasFuturas;

    @Bind(R.id.layout_aulas_feitas_click)
    View mLayoutAulasFuturasClick;

    @Bind(R.id.layout_aulas_feitas)
    View mLayoutAulasRealizadas;

    @Bind(R.id.lbl_view_more)
    TextView mLblViewMore;

    @Bind(R.id.lbl_nome)
    TextView mLblNome;

    @Bind(R.id.lbl_nr_aulas)
    TextView mLblNrAulas;

    @Bind(R.id.lbl_info_summary)
    TextView mLblInfoSummary;

    @Bind(R.id.lbl_validade)
    TextView mLblValidade;

    @Bind(R.id.lbl_datas_aulas_realizadas)
    TextView mLblDatasAulasRealizadas;

    @Bind(R.id.swipe_refresh_layout)
    PtrClassicFrameLayout mPtrClassicFrameLayout;

    @Bind(R.id.scroll_view)
    ScrollView mScrollView;

    private Student mStudent;
    private Teacher mTeacher;

    public static TeacherStudentFragment newInstance(Teacher teacher) {
        TeacherStudentFragment instructorStudentFragment = new TeacherStudentFragment();
        instructorStudentFragment.mTeacher = teacher;
        return instructorStudentFragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_teacher_student;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mStudent = SharedPrefManager.getInstance().getStudent();
        if (mStudent == null) {
            Navigator.navigateToLoginFragment(getContext());
        }

        if (isFirstScreen()) {
            configPtrClassicFrameLayout(mPtrClassicFrameLayout, mScrollView);
        } else {
            mPtrClassicFrameLayout.setPtrHandler(new PtrHandler() {
                @Override
                public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                    return false;
                }

                @Override
                public void onRefreshBegin(PtrFrameLayout frame) {
                }
            });
        }

        if (mTeacher.getSummary() != null) {
            processResult(mTeacher.getSummary());
        } else {
            getSummary();
        }
    }

    private boolean isFirstScreen() {
        if (getActivity() != null && ((BaseFragmentActivity)getActivity()).countFragments() == 1) {
            return true;
        }
        return false;
    }

    private void getSummary() {
        showLoadingView();
        StudentServiceEntry entry = new StudentServiceEntry();
        entry.setPhoneNumber(mStudent.getPhone());
        entry.setTeacherId(mTeacher.getObjectId());
        Call<SummaryResponse> service = RetrofitManager.getInstance().getStudentService().getSummary(entry);
        service.enqueue(new Callback<SummaryResponse>() {
            @Override
            public void onResponse(Call<SummaryResponse> call, Response<SummaryResponse> response) {
                if (response != null && response.errorBody() == null) {
                    processResult(response.body());
                } else {
                    showError(retryClick, cancelRetryClick);
                }
            }

            @Override
            public void onFailure(Call<SummaryResponse> call, Throwable t) {
                showError(retryClick, cancelRetryClick);
            }
        });
    }

    View.OnClickListener retryClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getSummary();
        }
    };

    private void processResult(Summary summary) {
        if (!isAdded()) {
            return;
        }
        if (summary != null) {
            if (summary.getRecentClasses() != null && !summary.getRecentClasses().isEmpty()) {
                String datas = ClassHelper.getDatas(summary.getRecentClasses());
                mLblDatasAulasRealizadas.setText(datas);
                mLayoutAulasRealizadas.setVisibility(View.VISIBLE);
                if (summary.getRecentClasses().size() >= Constants.LIMIT_RECENT_CLASSES) {
                    mLblViewMore.setVisibility(View.VISIBLE);
                    mLayoutAulasFuturasClick.setEnabled(true);
                    mLayoutAulasFuturasClick.setClickable(true);
                } else {
                    mLayoutAulasFuturasClick.setEnabled(false);
                    mLayoutAulasFuturasClick.setClickable(false);
                    mLblViewMore.setVisibility(View.GONE);
                }
            } else {
                mLayoutAulasRealizadas.setVisibility(View.GONE);
            }
            processAvaiableClasses(summary);

            if (Constants.TYPE_PAY_AFTER.equals(summary.getType())) {
                mLblNome.setText(getString(R.string.hello_xxx_paid_after, summary.getStudentName()));
                mLblInfoSummary.setText(R.string.classes_not_paid);
            } else {
                mLblNome.setText(getString(R.string.hello_xxx_paid_before, summary.getStudentName()));
                mLblInfoSummary.setText(R.string.avaiable_classes);
            }
        }
        mPtrClassicFrameLayout.setVisibility(View.VISIBLE);
        hideLoadingView();
    }

    private void processResult(SummaryResponse response) {
        processResult(response.getSummary());
    }

    private void processAvaiableClasses(Summary summary) {
        if (!isAdded()) {
            return;
        }
        Long qtde = summary.getQtClasses();
        Date expiredDate = summary.getDate();

        if (expiredDate != null) {
            mLblValidade.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(expiredDate));
        } else {
            mLblValidade.setText("-");
        }

        mLblNrAulas.setText(String.valueOf(qtde));
        mLayoutAulasFuturas.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();
        configToolbar();
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        if (isFirstScreen()) {
            return TOOLBAR_TYPE.NOTHING;
        }
        return TOOLBAR_TYPE.BACK_BUTTON;
    }

    @OnClick(R.id.layout_aulas_feitas_click)
    void aulasRealizadasClick() {
        Navigator.navigateToRecentClassesFragment(getContext(), mTeacher);
    }
}
