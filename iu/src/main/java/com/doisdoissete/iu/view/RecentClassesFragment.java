package com.doisdoissete.iu.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.doisdoissete.iu.R;
import com.doisdoissete.iu.library.helper.ClassHelper;
import com.doisdoissete.iu.library.model.MonthClazz;
import com.doisdoissete.iu.library.model.Student;
import com.doisdoissete.iu.library.model.Teacher;
import com.doisdoissete.iu.library.model.entry.StudentServiceEntry;
import com.doisdoissete.iu.library.model.response.ClassCustomApiResponse;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.doisdoissete.iu.util.SharedPrefManager;
import com.doisdoissete.iu.view.adapter.RecentClassesAdapter;
import com.doisdoissete.iu.view.base.BaseFragment;
import com.doisdoissete.iu.view.navigation.Navigator;

import java.util.ArrayList;

import butterknife.Bind;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 3/24/16.
 */
public class RecentClassesFragment extends BaseFragment {

    @Bind(R.id.recycler_view)
    RecyclerView mRecyclerView;

    RecentClassesAdapter mRecentClassesAdapter;

    private Student mStudent;
    private Teacher mTeacher;

    public static RecentClassesFragment newInstance(Teacher teacher) {
        RecentClassesFragment recentClassesFragment = new RecentClassesFragment();
        recentClassesFragment.mTeacher = teacher;
        return recentClassesFragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_recents_classes;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mStudent = SharedPrefManager.getInstance().getStudent();
        if (mStudent == null) {
            Navigator.navigateToLoginFragment(getContext());
        }

        initRecyclerView();
        getRencentClasses();
    }

    private void initRecyclerView() {
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mRecentClassesAdapter = new RecentClassesAdapter(new ArrayList<MonthClazz>());
        mRecyclerView.setAdapter(mRecentClassesAdapter);
    }

    private void getRencentClasses() {
        showLoadingView();

        StudentServiceEntry entry = new StudentServiceEntry();
        entry.setPhoneNumber(mStudent.getPhone());
        entry.setTeacherId(mTeacher.getObjectId());

        Call<ClassCustomApiResponse> service = RetrofitManager.getInstance().getClassService().getClassesDone(entry);
        service.enqueue(new Callback<ClassCustomApiResponse>() {
            @Override
            public void onResponse(Call<ClassCustomApiResponse> call, Response<ClassCustomApiResponse> response) {
                if (response != null && response.errorBody() == null) {
                    processRencentClasses(response.body());
                } else {
                    showError(retryClick, cancelRetryClick);
                }
            }

            @Override
            public void onFailure(Call<ClassCustomApiResponse> call, Throwable t) {
                showError(retryClick, cancelRetryClick);
            }
        });
    }

    View.OnClickListener retryClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getRencentClasses();
        }
    };

    private void processRencentClasses(ClassCustomApiResponse response) {
        if (!isAdded()) {
            return;
        }
        if (response != null && !response.isEmpty()) {
            mRecentClassesAdapter.addData(ClassHelper.getMonthClazz(response.getResults()));
        }

        hideLoadingView();
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}