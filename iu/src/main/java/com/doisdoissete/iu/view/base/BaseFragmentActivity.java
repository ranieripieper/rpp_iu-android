package com.doisdoissete.iu.view.base;

import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.doisdoissete.iu.R;
import com.doisdoissete.iu.library.view.custom.TouchBlackHoleView;

import butterknife.Bind;
import butterknife.ButterKnife;

public abstract class BaseFragmentActivity extends com.doisdoissete.iu.library.view.base.BaseFragmentActivity {

    @Nullable
    @Bind(R.id.include_layout_error)
    View mCustomError;

    @Nullable
    @Bind(R.id.lbl_msg_error)
    TextView mLblError;

    @Nullable
    @Bind(R.id.lbl_action_positive)
    TextView mLblPositiveButtonError;

    @Nullable
    @Bind(R.id.lbl_action_negative)
    TextView mLblNegativeButtonError;

    @Nullable
    @Bind(R.id.touch_black_hole_view)
    TouchBlackHoleView mTouchBlackHoleView;

    @Nullable
    @Bind(R.id.loading)
    protected ProgressBar mLoading;

    @Nullable
    @Bind(R.id.toolbar)
    protected Toolbar mToolbar;

    @Nullable
    @Bind(R.id.app_bar_layout)
    protected AppBarLayout mAppBarLayout;

    @Override
    protected ProgressBar getProgressBarLoading() {
        return mLoading;
    }

    @Override
    protected void executeInject() {
        ButterKnife.bind(this);
    }

    @Override
    protected Toolbar getToolbar() {
        return mToolbar;
    }

    @Nullable
    public AppBarLayout getAppBarLayout() {
        return mAppBarLayout;
    }

    @Override
    protected View getCustomError() {
        return mCustomError;
    }

    @Override
    protected TextView getLblError() {
        return mLblError;
    }

    @Override
    protected TextView getPositiveErrorButton() {
        return mLblPositiveButtonError;
    }

    @Override
    protected TextView getNegativeErrorButton() {
        return mLblNegativeButtonError;
    }


    @Override
    protected TouchBlackHoleView getTouchBlackHoleView() {
        return mTouchBlackHoleView;
    }


}
