package com.doisdoissete.iu.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.doisdoissete.iu.R;
import com.doisdoissete.iu.library.model.response.StudentsResponse;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.doisdoissete.iu.library.util.NumberUtil;
import com.doisdoissete.iu.util.SharedPrefManager;
import com.doisdoissete.iu.view.base.BaseFragment;
import com.doisdoissete.iu.view.navigation.Navigator;

import br.com.jansenfelipe.androidmask.MaskEditTextChangedListener;
import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 3/15/16.
 */
public class LoginFragment extends BaseFragment {

    @Bind(R.id.content)
    View layoutContent;

    @Bind(R.id.edt_number)
    EditText mEdtNumber;

    @Bind(R.id.bt_submit)
    Button btSubmit;

    @Bind(R.id.txt_msg)
    TextView mTxtMsg;

    @Bind(R.id.layout_text_msg)
    View mLayoutMsg;

    @Bind(R.id.layout_icon_error)
    View mLayoutIconMsg;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MaskEditTextChangedListener maskTEL = new MaskEditTextChangedListener("(##)#########", mEdtNumber);
        mEdtNumber.addTextChangedListener(maskTEL);

        mEdtNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                hidewError();
            }

            @Override
            public void afterTextChanged(Editable s) {
                validatePhoneNumber();
            }
        });
        mEdtNumber.clearFocus();
        btSubmit.setTranslationX(getScreenWidth());
        btSubmit.setVisibility(View.GONE);
        layoutContent.requestFocus();
    }

    @OnClick(R.id.bt_submit)
    void btSubmitClick() {
        synchronized (submit) {
            if (submit) {
                return;
            }
        }
        submit = true;
        hidewError();
        showLoadingView();
        String where = String.format("{\"phone\":%d}", NumberUtil.getPhoneNumber(mEdtNumber.getText().toString()));
        Call<StudentsResponse> studentService = RetrofitManager.getInstance().getStudentService().getStudent(where);
        studentService.enqueue(new Callback<StudentsResponse>() {
            @Override
            public void onResponse(Call<StudentsResponse> call, Response<StudentsResponse> response) {
                hideLoadingView();
                processResult(response.body());
            }

            @Override
            public void onFailure(Call<StudentsResponse> call, Throwable t) {
                hideLoadingView();
                showError();
            }
        });
    }

    private Boolean submit = false;

    private void processResult(StudentsResponse response) {
        if (isAdded()) {
            if (response == null || response.isEmpty()) {
                showEmpty();
            } else {
                hidewError();
                SharedPrefManager.getInstance().setStudent(response.getResults().get(0));
                Navigator.navigateToHomeFragment(getContext());
            }
        }
    }

    private void hidewError() {
        if (isAdded() && mLayoutIconMsg.getVisibility() == View.VISIBLE) {
            ViewCompat.animate(mLayoutMsg)
                    .setDuration(getResources().getInteger(R.integer.animation_duration_login))
                    .alpha(0f)
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            if (isAdded()) {
                                mLayoutMsg.setVisibility(View.INVISIBLE);
                            }
                        }
                    });
            ViewCompat.animate(mLayoutIconMsg)
                    .setDuration(getResources().getInteger(R.integer.animation_duration_login))
                    .alpha(0f)
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            if (isAdded()) {
                                mLayoutIconMsg.setVisibility(View.INVISIBLE);
                            }
                        }
                    });
        }
    }

    protected void showError() {
        showMessage(R.string.generic_error);
        synchronized (submit) {
            submit = false;
        }
    }

    private void showMessage(int msg) {
        if (isAdded()) {
            mLayoutMsg.setAlpha(0f);
            mLayoutIconMsg.setAlpha(0f);
            mTxtMsg.setText(msg);
            mLayoutMsg.setVisibility(View.VISIBLE);
            mLayoutIconMsg.setVisibility(View.VISIBLE);

            ViewCompat.animate(mLayoutMsg)
                    .setDuration(getResources().getInteger(R.integer.animation_duration_login))
                    .alpha(1f);
            ViewCompat.animate(mLayoutIconMsg)
                    .setDuration(getResources().getInteger(R.integer.animation_duration_login))
                    .alpha(1f);
        }
    }

    private void showEmpty() {
        Navigator.navigateToShareFragment(getContext());
        synchronized (submit) {
            submit = false;
        }
    }

    private int getScreenWidth() {
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        return metrics.widthPixels;
    }

    private void validatePhoneNumber() {
        if (phoneNumberIsValid()) {
            if (btSubmit.getVisibility() != View.VISIBLE) {
                showBtSubmit();
            }
        } else {
            if (btSubmit.getVisibility() == View.VISIBLE) {
                hiddeBtSubmit();
            }
        }
    }

    private void showBtSubmit() {
        btSubmit.setVisibility(View.VISIBLE);
        ViewCompat.animate(btSubmit)
                .setDuration(getResources().getInteger(R.integer.animation_duration_login))
                .translationX(0);
    }

    private void hiddeBtSubmit() {
        ViewCompat.animate(btSubmit)
                .setDuration(getResources().getInteger(R.integer.animation_duration_login))
                .translationX(getScreenWidth())
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        btSubmit.setVisibility(View.GONE);
                    }
                });
    }

    private boolean phoneNumberIsValid() {
        String phoneNumber = NumberUtil.getPhoneNumberStr(mEdtNumber.getText().toString());
        if (!TextUtils.isEmpty(phoneNumber) && phoneNumber.length() >= 10) {
            return true;
        }
        return false;
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_login;
    }
}
