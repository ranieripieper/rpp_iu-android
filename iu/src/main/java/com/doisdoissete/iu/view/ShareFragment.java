package com.doisdoissete.iu.view;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.doisdoissete.iu.R;
import com.doisdoissete.iu.library.util.FunctionsUtil;
import com.doisdoissete.iu.view.base.BaseFragment;
import com.doisdoissete.iu.view.navigation.Navigator;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ranipieper on 4/26/16.
 */
public class ShareFragment extends BaseFragment {

    @Bind(R.id.img_envelope)
    ImageView mImgEnvelope;

    private Handler mHandler = new Handler();
    Timer mTimer = new Timer();

    private static final int DURATION_SWING_ENVELOPE = 500;
    private static final int INTERVAL_ANIM_ENVELOPE = DURATION_SWING_ENVELOPE + 2000;

    public static ShareFragment newInstance() {
        return new ShareFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startEnvelopeAnimation();
            }
        }, 500);
        hideKeyboard();
    }

    private void startEnvelopeAnimation() {
        mImgEnvelope.setImageResource(R.drawable.envelope);
        AnimationDrawable animationDrawable = ((AnimationDrawable) mImgEnvelope.getDrawable());
        animationDrawable.start();

        long totalDuration = 0;
        for (int i = 0; i < animationDrawable.getNumberOfFrames(); i++) {
            totalDuration += animationDrawable.getDuration(i);
        }

        mTimer.schedule(getTimerTask(), totalDuration);
    }


    private TimerTask getTimerTask() {
        return new TimerTask() {
            @Override
            public void run() {
                swingEnvelope();
                mTimer.schedule(getTimerTask(), INTERVAL_ANIM_ENVELOPE);
            }
        };
    }

    private void swingEnvelope() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (isAdded()) {
                    YoYo.with(Techniques.Swing)
                            .duration(DURATION_SWING_ENVELOPE)
                            .playOn(mImgEnvelope);
                }
            }
        });
    }

    @OnClick(R.id.bt_share)
    void btShareClick() {
        hideKeyboard();
        FunctionsUtil.shareUrl(getContext(), getString(R.string.share_text_url), getString(R.string.APP_NAME));
    }

    @OnClick(R.id.txt_dont_invite_now)
    void btDontInviteNowClick() {
        Navigator.navigateToLoginFragment(getContext());
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_share;
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.NOTHING;
    }

    @Override
    protected void onBackPressed() {
        if (mTimer != null) {
            mTimer.cancel();
        }
        super.onBackPressed();
    }
}
