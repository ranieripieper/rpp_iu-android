package com.doisdoissete.iu.view;

import android.content.Context;
import android.content.Intent;

import com.doisdoissete.iu.view.navigation.Navigator;

/**
 * Created by ranipieper on 4/27/16.
 */
public class SplashScreenActivity extends com.doisdoissete.iu.library.view.SplashScreenActivity {

    @Override
    protected void navigateToMain() {
        Navigator.navigateToMain(SplashScreenActivity.this);
    }

    public static Intent getActivityIntent(Context context) {
        Intent it = new Intent(context, SplashScreenActivity.class);
        return it;
    }
}
