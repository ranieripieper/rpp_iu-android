package com.doisdoissete.iu.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.doisdoissete.iu.R;
import com.doisdoissete.iu.library.model.Student;
import com.doisdoissete.iu.library.model.Teacher;
import com.doisdoissete.iu.library.model.entry.StudentServiceEntry;
import com.doisdoissete.iu.library.model.response.TeachersResponse;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.doisdoissete.iu.util.SharedPrefManager;
import com.doisdoissete.iu.view.adapter.TeachersAdapter;
import com.doisdoissete.iu.view.base.BaseFragment;
import com.doisdoissete.iu.view.navigation.Navigator;
import com.parse.ParseInstallation;

import butterknife.Bind;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 3/17/16.
 */
public class HomeFragment extends BaseFragment {

    @Bind(R.id.swipe_refresh_layout)
    PtrClassicFrameLayout mPtrClassicFrameLayout;

    @Bind(R.id.recycler_view)
    RecyclerView mRecyclerView;

    TeachersAdapter mTeachersAdapter;

    private Student mStudent;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mStudent = SharedPrefManager.getInstance().getStudent();
        if (mStudent == null) {
            Navigator.navigateToLoginFragment(getContext());
        } else {
            getTeachers();
        }
    }

    private void getTeachers() {
        showLoadingView();
        StudentServiceEntry entry = new StudentServiceEntry();
        entry.setPhoneNumber(mStudent.getPhone());
        entry.setInstallationId(ParseInstallation.getCurrentInstallation().getInstallationId());
        Call<TeachersResponse> service = RetrofitManager.getInstance().getStudentService().getTeachers(entry);
        service.enqueue(new Callback<TeachersResponse>() {
            @Override
            public void onResponse(Call<TeachersResponse> call, Response<TeachersResponse> response) {
                if (response.errorBody() == null && response.body() != null && !response.body().isEmpty()) {
                    processResult(response.body());
                } else {
                    showError(retryClick);
                }
            }

            @Override
            public void onFailure(Call<TeachersResponse> call, Throwable t) {
                showError(retryClick);
            }
        });
    }

    View.OnClickListener retryClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getTeachers();
        }
    };

    private void processResult(TeachersResponse teachersResponse) {
        if (teachersResponse.getResults().size() == 1) {
            Navigator.navigateToTeacherStudentFragment(getContext(), teachersResponse.getResults().get(0), true);
        } else {
            mRecyclerView.setHasFixedSize(true);

            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);

            mTeachersAdapter = new TeachersAdapter(teachersResponse.getResults(), new TeachersAdapter.TeacherAdapterListener() {
                @Override
                public void onTeacherClick(Teacher teacher, int position) {
                    Navigator.navigateToTeacherStudentFragment(getContext(), teacher);
                }
            });

            mRecyclerView.setAdapter(mTeachersAdapter);

            configPtrClassicFrameLayout(mPtrClassicFrameLayout, mRecyclerView);
            mPtrClassicFrameLayout.setVisibility(View.VISIBLE);
            hideLoadingView();
        }
    }
}
