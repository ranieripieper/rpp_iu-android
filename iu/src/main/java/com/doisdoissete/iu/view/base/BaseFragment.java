package com.doisdoissete.iu.view.base;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.doisdoissete.iu.R;
import com.doisdoissete.iu.library.model.Student;
import com.doisdoissete.iu.service.background.LogoutService;
import com.doisdoissete.iu.util.SharedPrefManager;
import com.doisdoissete.iu.view.navigation.Navigator;

import butterknife.Bind;
import butterknife.ButterKnife;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

/**
 * Created by ranipieper on 11/11/15.
 */
public abstract class BaseFragment extends com.doisdoissete.iu.library.view.base.BaseFragment {

    @Nullable
    @Bind(R.id.loading)
    ProgressBar mLoading;

    @Nullable
    @Bind(R.id.include_layout_error)
    View mCustomError;

    @Nullable
    @Bind(R.id.lbl_msg_error)
    TextView mLblError;

    @Nullable
    @Bind(R.id.lbl_action_positive)
    TextView mLblPositiveButtonError;

    @Nullable
    @Bind(R.id.lbl_action_negative)
    TextView mLblNegativeButtonError;

    @Override
    protected ProgressBar getProgressBarLoading() {
        return mLoading;
    }

    @Override
    protected void executeInject(View view) {
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    protected void configPtrClassicFrameLayout(final PtrClassicFrameLayout ptrClassicFrameLayout, final View scrollView) {
        ptrClassicFrameLayout.setRotation(180);
        scrollView.setRotation(180);

        View header = LayoutInflater.from(getContext()).inflate(R.layout.include_exit, null);
        header.setRotation(180);
        ptrClassicFrameLayout.setHeaderView(header);

        if (scrollView instanceof RecyclerView) {
            ptrClassicFrameLayout.setPtrHandler(new PtrHandler() {
                @Override
                public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                    return !ViewCompat.canScrollVertically(scrollView, 1);
                }

                @Override
                public void onRefreshBegin(PtrFrameLayout frame) {
                    ptrClassicFrameLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            logout();
                        }
                    }, 150);
                }
            });
        } else {
            ptrClassicFrameLayout.setPtrHandler(new PtrHandler() {
                @Override
                public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                    return PtrDefaultHandler.checkContentCanBePulledDown(frame, scrollView, header);
                }

                @Override
                public void onRefreshBegin(PtrFrameLayout frame) {
                    ptrClassicFrameLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            logout();
                        }
                    }, 150);
                }
            });
        }
    }

    protected void logout() {
        Student student = SharedPrefManager.getInstance().getStudent();
        if (student != null) {
            Intent serviceIntent = new Intent(getActivity(), LogoutService.class);
            serviceIntent.putExtra(LogoutService.EXTRA_PHONE_NUMBER, student.getPhone());
            getActivity().startService(serviceIntent);
        }
        SharedPrefManager.getInstance().setStudent(null);
        Navigator.navigateToLoginFragment(getContext());
    }

    @Override
    protected View getCustomError() {
        return mCustomError;
    }

    @Override
    protected TextView getLblError() {
        return mLblError;
    }

    @Override
    protected TextView getPositiveErrorButton() {
        return mLblPositiveButtonError;
    }

    @Override
    protected TextView getNegativeErrorButton() {
        return mLblNegativeButtonError;
    }
}
