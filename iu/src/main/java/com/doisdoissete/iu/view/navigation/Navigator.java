package com.doisdoissete.iu.view.navigation;

import android.content.Context;
import android.content.Intent;

import com.doisdoissete.iu.library.model.Teacher;
import com.doisdoissete.iu.library.view.base.BaseFragmentActivity;
import com.doisdoissete.iu.view.HomeFragment;
import com.doisdoissete.iu.view.LoginFragment;
import com.doisdoissete.iu.view.MainActivity;
import com.doisdoissete.iu.view.RecentClassesFragment;
import com.doisdoissete.iu.view.ShareFragment;
import com.doisdoissete.iu.view.TeacherStudentFragment;

/**
 * Created by ranipieper on 3/15/16.
 */
public class Navigator {

    public static void navigateToMain(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, MainActivity.class));
        }
    }

    public static void navigateToLoginFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(LoginFragment.newInstance(), true);
        }
    }

    public static void navigateToHomeFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(HomeFragment.newInstance(), true);
        }
    }

    public static void navigateToRecentClassesFragment(Context context, Teacher teacher) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(RecentClassesFragment.newInstance(teacher), false);
        }
    }

    public static void navigateToShareFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(ShareFragment.newInstance(), true);
        }
    }

    public static void navigateToTeacherStudentFragment(Context context, Teacher teacher) {
        navigateToTeacherStudentFragment(context, teacher, false);
    }

    public static void navigateToTeacherStudentFragment(Context context, Teacher teacher, boolean cleanStack) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(TeacherStudentFragment.newInstance(teacher), cleanStack);
        }
    }
}
