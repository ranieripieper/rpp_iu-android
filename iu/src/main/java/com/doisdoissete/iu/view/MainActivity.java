package com.doisdoissete.iu.view;

import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;

import com.doisdoissete.iu.R;
import com.doisdoissete.iu.util.SharedPrefManager;
import com.doisdoissete.iu.view.base.BaseFragmentActivity;
import com.doisdoissete.iu.view.navigation.Navigator;

public class MainActivity extends BaseFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (SharedPrefManager.getInstance().getStudent() != null) {
            Navigator.navigateToHomeFragment(getContext());
        } else {
            Navigator.navigateToLoginFragment(getContext());
        }

        //ReceiverPush.showNotification(getContext(), null, "teste 2", R.drawable.ic_stat_iu);
    }

    @Override
    protected int getLayoutFragmentContainer() {
        return R.id.layout_fragment_container;
    }

    public void setNavBackMode() {
        setNavBackMode(ResourcesCompat.getDrawable(getResources(), R.drawable.back_chevron, null), getDrawableBackModeListener());
    }


}
