package com.doisdoissete.iu.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.doisdoissete.iu.library.model.Student;
import com.doisdoissete.iu.library.service.RetrofitManager;
import com.google.gson.JsonSyntaxException;

public class SharedPrefManager {

    private static SharedPrefManager sharedPrefManager;
    private Context mContext;

    private static final String PREFERENCES = SharedPrefManager.class + "";

    private static final String STUDENT = "STUDENT";

    private SharedPrefManager(Context context) {
        this.mContext = context;
    }

    public static SharedPrefManager getInstance() {
        return sharedPrefManager;
    }

    public static void init(Application application) {
        sharedPrefManager = new SharedPrefManager(application);
    }

    private SharedPreferences getSharedPreferences() {
        return mContext.getSharedPreferences(PREFERENCES, 0);
    }

    public Student getStudent() {
        String json = getSharedPreferences().getString(STUDENT, null);
        try {
            if (!TextUtils.isEmpty(json)) {
                return RetrofitManager.getGson().fromJson(json, Student.class);
            }
        } catch (JsonSyntaxException e) {
        }
        return null;
    }

    public void setStudent(Student value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(STUDENT, RetrofitManager.getGson().toJson(value));
        editor.commit();
    }


    private SharedPreferences.Editor getEditor() {
        return getSharedPreferences().edit();
    }

}
